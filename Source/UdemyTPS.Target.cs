// https://m.vk.com/id357239644

using UnrealBuildTool;
using System.Collections.Generic;

public class UdemyTPSTarget : TargetRules
{
	public UdemyTPSTarget(TargetInfo Target) : base(Target)
	{
		Type = TargetType.Game;
		DefaultBuildSettings = BuildSettingsVersion.V2;

		ExtraModuleNames.AddRange( new string[] { "UdemyTPS" } );
	}
}
