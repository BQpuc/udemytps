// https://m.vk.com/id357239644

using UnrealBuildTool;
using System.Collections.Generic;

public class UdemyTPSEditorTarget : TargetRules
{
	public UdemyTPSEditorTarget(TargetInfo Target) : base(Target)
	{
		Type = TargetType.Editor;
		DefaultBuildSettings = BuildSettingsVersion.V2;

		ExtraModuleNames.AddRange( new string[] { "UdemyTPS" } );
	}
}
