// https://m.vk.com/id357239644

using UnrealBuildTool;

public class UdemyTPS : ModuleRules
{
	public UdemyTPS(ReadOnlyTargetRules Target) : base(Target)
	{
		PCHUsage = PCHUsageMode.UseExplicitOrSharedPCHs;
	
		PublicDependencyModuleNames.AddRange(new string[] { 
			"Core", 
			"CoreUObject", 
			"Engine", 
			"InputCore", 
			"Niagara", 
			"PhysicsCore",
			"GameplayTasks", 
			"NavigationSystem"
		});

		PrivateDependencyModuleNames.AddRange(new string[] {  });

		PublicIncludePaths.AddRange(new string[] { 
			"UdemyTPS/Public/Player",
			"UdemyTPS/Public/Components", 
			"UdemyTPS/Public/Develop",
			"UdemyTPS/Public/UI",
			"UdemyTPS/Public/Weapon"});

		// Uncomment if you are using Slate UI
		// PrivateDependencyModuleNames.AddRange(new string[] { "Slate", "SlateCore" });
		
		// Uncomment if you are using online features
		// PrivateDependencyModuleNames.Add("OnlineSubsystem");

		// To include OnlineSubsystemSteam, add it to the plugins section in your uproject file with the Enabled attribute set to true
	}
}
