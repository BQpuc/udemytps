// https://m.vk.com/id357239644

#include "Components/RespawnComponent.h"
#include "TPSGameModeBase.h"

URespawnComponent::URespawnComponent()
{
  PrimaryComponentTick.bCanEverTick = false;
}

void URespawnComponent::BeginPlay()
{
  Super::BeginPlay();
}

void URespawnComponent::Respawn(int32 RespawnTime)
{
  if (!GetWorld())
    return;

  RespawnCountDown = RespawnTime;
  GetWorld()->GetTimerManager().SetTimer(RespawnHandle,          //
      this, &URespawnComponent::RespawnTimerUpdate, 1.0f, true); //
}

bool URespawnComponent::IsRespawnInProgress() const
{
  return GetWorld() && GetWorld()->GetTimerManager().IsTimerActive(RespawnHandle);
}

void URespawnComponent::RespawnTimerUpdate()
{
  if (--RespawnCountDown <= 0)
  {
    if (!GetWorld() && !RespawnHandle.IsValid())
      return;
    GetWorld()->GetTimerManager().ClearTimer(RespawnHandle);

    ATPSGameModeBase* GameMode = Cast<ATPSGameModeBase>(GetWorld()->GetAuthGameMode());
    if (!GameMode)
      return;

    GameMode->RespawnRequest(Cast<AController>(GetOwner()));
  }
}
