// https://m.vk.com/id357239644

#include "Components/Pickups/BasePickup.h"
#include "Components/SphereComponent.h"
#include "Kismet/GameplayStatics.h"
#include "Sound/SoundCue.h"

DEFINE_LOG_CATEGORY_STATIC(PickupLog, All, All);

// Sets default values
ABasePickup::ABasePickup()
{
  // Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
  PrimaryActorTick.bCanEverTick = true;

  CollisionComponent = CreateDefaultSubobject<USphereComponent>("CollisionComponent");
  CollisionComponent->InitSphereRadius(50.0f);
  CollisionComponent->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
  CollisionComponent->SetCollisionResponseToAllChannels(ECollisionResponse::ECR_Overlap);
  SetRootComponent(CollisionComponent);
}

// Called when the game starts or when spawned
void ABasePickup::BeginPlay()
{
  Super::BeginPlay();
  InitLocation = GetActorLocation();
  GenerateRotationYaw();
}

// Called every frame
void ABasePickup::Tick(float DeltaTime)
{
  Super::Tick(DeltaTime);
  SinMove();
  if (RandMove.IsRotating)
  AddActorLocalRotation(FRotator(0.0f, RotationYaw, 0.0f));
}

void ABasePickup::NotifyActorBeginOverlap(AActor* OtherActor)
{
  Super::NotifyActorBeginOverlap(OtherActor);

  APawn* Pawn = Cast<APawn>(OtherActor);
  if (GivePickupTo(Pawn))
  {
    if (TakeSound)
      UGameplayStatics::PlaySoundAtLocation(GetWorld(), TakeSound, GetActorLocation());
    PickupWasTaken();
  }
    
}

bool ABasePickup::CouldBeTaken() const
{
  return !GetWorldTimerManager().IsTimerActive(L_RespawnTH);
}

bool ABasePickup::GivePickupTo(APawn* Pawn)
{
  return false;
}

void ABasePickup::GenerateRotationYaw()
{
  float Direction = FMath::RandBool() ? 1.0f : -1.0f;
  RotationYaw = FMath::RandRange(RandMove.MinAngle, RandMove.MaxAngle) * Direction;
}

void ABasePickup::SinMove()
{
  if (!RandMove.IsSinMove)
    return;
  FVector CurrentLocation = GetActorLocation();
  if (GetWorld())
  {
    float time = GetWorld()->GetTimeSeconds();
    CurrentLocation.Z = InitLocation.Z + RandMove.Amplitude * FMath::Sin(RandMove.Frequency * time);
    SetActorLocation(CurrentLocation);
  }
}

void ABasePickup::PickupWasTaken()
{
  CollisionComponent->SetCollisionResponseToAllChannels(ECollisionResponse::ECR_Ignore);
  GetRootComponent()->SetVisibility(false, true);
 
  GetWorldTimerManager().SetTimer(L_RespawnTH, this, &ABasePickup::Respawn, RespawnTime);
}

void ABasePickup::Respawn()
{
  GenerateRotationYaw();
  CollisionComponent->SetCollisionResponseToAllChannels(ECollisionResponse::ECR_Overlap);
  GetRootComponent()->SetVisibility(true, true);
}
