// https://m.vk.com/id357239644


#include "Components/Pickups/AmmoPickup.h"
#include "Weapon/TPSWeapon.h"
#include "Components/HealthComponent.h"
#include "Components/WeaponComponent.h"


DEFINE_LOG_CATEGORY_STATIC(AmmoPickupLog, All, All);

bool AAmmoPickup::GivePickupTo(APawn* Pawn)
{
  const UHealthComponent* HealthComp =  UTypes::GetPlayerComponent<UHealthComponent>(Pawn);
  if (!HealthComp || HealthComp->GetIsDead())
    return false;
  UWeaponComponent* WeaponComp = UTypes::GetPlayerComponent<UWeaponComponent>(Pawn);
  if (!WeaponComp)
    return false;

  return WeaponComp->TryToAddAmmo(WeaponType, ClipsAmount);
}