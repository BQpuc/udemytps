// https://m.vk.com/id357239644


#include "Components/Pickups/HealthPickup.h"
#include "Components/HealthComponent.h"

DEFINE_LOG_CATEGORY_STATIC(HealthPickupLog, All, All);

bool AHealthPickup::GivePickupTo(APawn* Pawn)
{
 UHealthComponent* HealthComp = UTypes::GetPlayerComponent<UHealthComponent>(Pawn);
 if (!HealthComp || HealthComp->GetIsDead())
    return false;

  return HealthComp->RegenPercent(myVariable);
}