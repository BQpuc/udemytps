// https://m.vk.com/id357239644

#include "Components/AIWeaponComponent.h"
#include "Weapon/TPSWeapon.h"

void UAIWeaponComponent::StartFire()
{
  if (!CanFire())
    return;
  if (CurrentWeapon->IsAmmoEmpty())
  {
    NextWeapon();
  }
  else
  {
    CurrentWeapon->StartFire();
  }
}

void UAIWeaponComponent::NextWeapon()
{
  if (!CanEquip())
    return;
  int32 NextIndex = (CurrentWeaponIndex + 1) % Weapons.Num();
  while (NextIndex != CurrentWeaponIndex)
  {
    if (!Weapons[NextIndex]->IsAmmoEmpty())
      break;
    NextIndex = (NextIndex + 1) % Weapons.Num();
  }
  if (CurrentWeaponIndex != NextIndex)      // ����� ������ ������
  {
    CurrentWeaponIndex = NextIndex;
    EquipWeapon(CurrentWeaponIndex);
  }
}

void UAIWeaponComponent::ToDefaultSMix() {}

void UAIWeaponComponent::ExploseSound() {}
