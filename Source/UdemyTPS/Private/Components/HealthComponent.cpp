// https://m.vk.com/id357239644

#include "Components/HealthComponent.h"
#include "Develop/DamageType/FireDT.h"
#include "Develop/DamageType/IceDT.h"
#include "Develop/DamageType/LightDT.h"
#include "Develop/DamageType/PhysicalDT.h"
#include "Develop/DamageType/PoisonDT.h"
#include "Develop/TPSGameInstance.h"
#include "Engine/World.h"
#include "GameFramework/Character.h"
#include "GameFramework/Controller.h"
#include "Camera/CameraShakeBase.h"
#include "TPSGameModeBase.h"
#include "PhysicalMaterials/PhysicalMaterial.h"
#include "Perception/AISense_Damage.h"

DEFINE_LOG_CATEGORY_STATIC(HealthLog, All, All)

// Sets default values for this component's properties
UHealthComponent::UHealthComponent()
{
  PrimaryComponentTick.bCanEverTick = false;
}

void UHealthComponent::BeginPlay()
{
  Super::BeginPlay();

  Health = MaxHealth;
  AActor* ComponentOwner = GetOwner();
  if (ComponentOwner)
  {
    ComponentOwner->OnTakeAnyDamage.AddDynamic(this, &UHealthComponent::AnyDamage);
    ComponentOwner->OnTakePointDamage.AddDynamic(this, &UHealthComponent::OnTakePointDamage);
    ComponentOwner->OnTakeRadialDamage.AddDynamic(this, &UHealthComponent::OnTakeRadialDamage);
  }
}

void UHealthComponent::OnTakePointDamage(AActor* DamagedActor, float Damage, AController* InstigatedBy, FVector HitLocation,
    UPrimitiveComponent* FHitComponent, FName BoneName, FVector ShotFromDirection, const UDamageType* DamageType, AActor* DamageCauser)
{
  float FinalDamage = Damage * GetPointDamageModifier(DamagedActor, BoneName);
  ApplyDamage(FinalDamage, InstigatedBy);
}

void UHealthComponent::OnTakeRadialDamage(AActor* DamagedActor, float Damage, const UDamageType* DamageType, FVector Origin,
    FHitResult HitInfo, AController* InstigatedBy, AActor* DamageCauser)
{
  ApplyDamage(Damage, InstigatedBy);
}

void UHealthComponent::ApplyDamage(float Damage, AController* InstigatedBy)
{
  if (bIsDead || Health <= 0.0f)
    return;
  KillerContr = InstigatedBy;
  ChangeHealth(Damage);
  ChangeHealth(0.0f);
  PlayCameraShake();
  ReportDamageEvent(Damage, InstigatedBy);
}

float UHealthComponent::GetPointDamageModifier(AActor* DamageActor, const FName& BoneName)
{
  const auto Character = Cast<ACharacter>(DamageActor);
  if (!Character || !Character->GetMesh() || !Character->GetMesh()->GetBodyInstance(BoneName))
    return 1.0f; // �������� ���������� �������� �� ����
  const auto PhysMaterial = Character->GetMesh()->GetBodyInstance(BoneName)->GetSimplePhysicalMaterial();

  if (!PhysMaterial || !DamageModifier.Contains(PhysMaterial)) // ���������, ���������� �� ����
    return 1.0f;
  // ���������� �������� ������������
  return DamageModifier[PhysMaterial];
}

void UHealthComponent::TimerFire()
{
  if (FireTime <= 0.0f)
  {
    FireFlag = false;
    GetWorld()->GetTimerManager().ClearTimer(FireHandle);
    // UE_LOG(HealthLog, Display, TEXT("Debuff off FireTime %.1f"), FireTime);
  }
  else
  {
    FireTime -= FireTick;
    ChangeHealth(FireTickDmg);
    // UE_LOG(HealthLog, Display, TEXT("TimerFire %.1f"), Health);
    if (bIsDead)
      GetWorld()->GetTimerManager().ClearTimer(FireHandle);
  }
}

void UHealthComponent::TimerIce()
{
  if (IceTime <= 0.0f)
  {
    IceFlag = false;
    GetWorld()->GetTimerManager().ClearTimer(IceHandle);
    // UE_LOG(HealthLog, Display, TEXT("Debuff off IceTime %.1f"), IceTime);
  }
  else
  {
    IceTime -= IceTick;
    ChangeHealth(IceTickDmg);
    // UE_LOG(HealthLog, Display, TEXT("TimerIce %.1f"), Health);
    if (bIsDead)
      GetWorld()->GetTimerManager().ClearTimer(IceHandle);
  }
}

void UHealthComponent::TimerLight()
{
  if (LightTime <= 0.0f)
  {
    LightFlag = false;
    GetWorld()->GetTimerManager().ClearTimer(LightHandle);
    // UE_LOG(HealthLog, Display, TEXT("Debuff off LightTime %.1f"), LightTime);
  }
  else
  {
    LightTime -= LightTick;
    ChangeHealth(LightTickDmg);
    // UE_LOG(HealthLog, Display, TEXT("TimerLight %.1f"), Health);
    if (bIsDead)
      GetWorld()->GetTimerManager().ClearTimer(LightHandle);
  }
}

void UHealthComponent::TimerDark()
{
  if (DarkTime <= 0.0f)
  {
    DarkFlag = false;
    GetWorld()->GetTimerManager().ClearTimer(DarkHandle);
    // UE_LOG(HealthLog, Display, TEXT("Debuff off DarkTime %.1f"), DarkTime);
  }
  else
  {
    DarkTime -= DarkTick;
    ChangeHealth(DarkTickDmg);
    // UE_LOG(HealthLog, Display, TEXT("TimerDark %.1f"), Health);
    if (bIsDead)
      GetWorld()->GetTimerManager().ClearTimer(DarkHandle);
  }
}

void UHealthComponent::TimerPoison()
{
  if (PoisonTime <= 0.0f)
  {
    PoisonFlag = false;
    GetWorld()->GetTimerManager().ClearTimer(PoisonHandle);
    // UE_LOG(HealthLog, Display, TEXT("Debuff off PoisonTime %.1f"), PoisonTime);
  }
  else
  {
    PoisonTime -= PoisonTick;
    ChangeHealth(PoisonTickDmg);
    // UE_LOG(HealthLog, Display, TEXT("TimerPoison %.1f"), Health);
    if (bIsDead)
      GetWorld()->GetTimerManager().ClearTimer(PoisonHandle);
  }
}

void UHealthComponent::PlayCameraShake()
{
  if (bIsDead)
    return;
  APawn* Player = Cast<APawn>(GetOwner());
  if (!Player)
    return;
  APlayerController* Controller = Player->GetController<APlayerController>();
  if (!Controller || !Controller->PlayerCameraManager)
    return;

  Controller->PlayerCameraManager->StartCameraShake(CameraShake);
}

void UHealthComponent::Killed(AController* KillerController)
{
  if (!GetWorld())
    return;
  ATPSGameModeBase* GameMode = Cast<ATPSGameModeBase>(GetWorld()->GetAuthGameMode());
  if (!GameMode)
    return;
  APawn* Player = Cast<APawn>(GetOwner());
  AController* VictimController = Player ? Player->Controller : nullptr;

  GameMode->Killed(KillerController, VictimController);
}

void UHealthComponent::ChangeHealth(float Damage)
{

  if (bIsDead)
  {
    UE_LOG(HealthLog, Display, TEXT("bIsDead: %d"), bIsDead);
    return;
  }
  if (Health <= 0.0f)
  {
    if (KillerContr)
      Killed(KillerContr);
    OnDeath.Broadcast();
    bIsDead = true;
  }
  // Regen Timer
  if (bAutoHeal)
  {
    if (Damage > 0.0f)
    {
      RegenTime = HealDelay;
    }
    // UE_LOG(HealthLog, Display, TEXT("RegenTime: %.2f Health: %0.2f bRegenFlag : % d"), RegenTime, Health, bRegenFlag);
    if (!bRegenFlag && (Health < MaxHealth))
    {
      bRegenFlag = true;
      GetWorld()->GetTimerManager().SetTimer(RegenHandle, this, &UHealthComponent::Regeneration, HealUpdateTime, true);

      // UE_LOG(HealthLog, Display, TEXT("=== REGENERATION BEGIN ==="));
    }
  }
  Health = FMath::Clamp(Health - Damage, 0.0f, MaxHealth);
  OnChangeHealth.Broadcast(Health, Damage);
}

bool UHealthComponent::RegenPercent(float Percent)
{
  if (Health >= MaxHealth || Percent <= 0.0f || bIsDead)
    return false;

  float LocPercent = FMath::Clamp(Percent, 0.0f, 1.0f);
  Health = FMath::Clamp(Health + MaxHealth * LocPercent, 0.0f, MaxHealth);
  return true;
}

void UHealthComponent::ReportDamageEvent(float Damage, AController* InstigatedBy)
{
  if (!InstigatedBy || !InstigatedBy->GetPawn() || !GetOwner())
    return;
  // �������� ���������� � ��������� ����� � Perception Comp
  UAISense_Damage::ReportDamageEvent(GetWorld(),   //
      GetOwner(),                                  //
      InstigatedBy->GetPawn(),                     //
      Damage,                                      //
      InstigatedBy->GetPawn()->GetActorLocation(), //
      GetOwner()->GetActorLocation());
}

void UHealthComponent::Regeneration()
{
  if (RegenTime <= 0.0f)
  {
    if (Health >= MaxHealth)
    {
      bRegenFlag = false;
      GetWorld()->GetTimerManager().ClearTimer(RegenHandle);
    }
    ChangeHealth(-HealRegen);
    // UE_LOG(HealthLog, Display, TEXT("Regeneration %.1f"), HealRegen);
  }
  else
  {
    if (bIsDead)
      GetWorld()->GetTimerManager().ClearTimer(RegenHandle);
    RegenTime -= HealUpdateTime;
    // UE_LOG(HealthLog, Display, TEXT("Regen Time: %.2f"), RegenTime);
  }
}

void UHealthComponent::AnyDamage(
    AActor* DamagedActor, float Damage, const class UDamageType* DamageType, class AController* InstigatedBy, AActor* DamageCauser)
{
  if (bIsDead || Health <= 0.0f)
    return;
  UTPSGameInstance* myGI = Cast<UTPSGameInstance>(DamageCauser->GetGameInstance());
  if (myGI)
  {
    FDebuff myDebuff;
    if (DamageType && GetWorld())
    {
      if (DamageType->IsA<UFireDT>())
      {
        myGI->GetDebuffInfoByName(FireDbf, myDebuff);
        DelDamage.Broadcast(EDamageType::Fire, myDebuff);
        FireTime = myDebuff.DebuffMaxTime;
        if (!FireFlag)
        {
          FireTickDmg = myDebuff.DamagePerTick;
          FireMaxTime = myDebuff.DebuffMaxTime;
          FireFlag = true;
          FireTick = myDebuff.TickInterval;
          GetWorld()->GetTimerManager().SetTimer(FireHandle, this, &UHealthComponent::TimerFire, FireTick, true);
          // UE_LOG(HealthLog, Display, TEXT("Begin Fire"));
        }
      }
      if (DamageType->IsA<UIceDT>())
      {
        myGI->GetDebuffInfoByName(IceDbf, myDebuff);
        DelDamage.Broadcast(EDamageType::Ice, myDebuff);
        IceTime = myDebuff.DebuffMaxTime;
        if (!IceFlag)
        {
          IceTickDmg = myDebuff.DamagePerTick;
          IceMaxTime = myDebuff.DebuffMaxTime;
          IceFlag = true;
          IceTick = myDebuff.TickInterval;
          GetWorld()->GetTimerManager().SetTimer(IceHandle, this, &UHealthComponent::TimerIce, IceTick, true);
          // UE_LOG(HealthLog, Display, TEXT("Begin Ice"));
        }
      }
      if (DamageType->IsA<ULightDT>())
      {
        myGI->GetDebuffInfoByName(LightDbf, myDebuff);
        DelDamage.Broadcast(EDamageType::Light, myDebuff);
        LightTime = myDebuff.DebuffMaxTime;
        if (!LightFlag)
        {
          LightTickDmg = myDebuff.DamagePerTick;
          LightMaxTime = myDebuff.DebuffMaxTime;
          LightFlag = true;
          LightTick = myDebuff.TickInterval;
          GetWorld()->GetTimerManager().SetTimer(LightHandle, this, &UHealthComponent::TimerLight, LightTick, true);
          // UE_LOG(HealthLog, Display, TEXT("Begin Light"));
        }
      }
      if (DamageType->IsA<UPhysicalDT>())
      {
        myGI->GetDebuffInfoByName(DarkDbf, myDebuff);
        DelDamage.Broadcast(EDamageType::Dark, myDebuff);
        DarkTime = myDebuff.DebuffMaxTime;
        if (!DarkFlag)
        {
          DarkTickDmg = myDebuff.DamagePerTick;
          DarkMaxTime = myDebuff.DebuffMaxTime;
          DarkFlag = true;
          DarkTick = myDebuff.TickInterval;
          GetWorld()->GetTimerManager().SetTimer(DarkHandle, this, &UHealthComponent::TimerDark, DarkTick, true);
          // UE_LOG(HealthLog, Display, TEXT("Begin Dark"));
        }
      }
      if (DamageType->IsA<UPoisonDT>())
      {
        myGI->GetDebuffInfoByName(PoisonDbf, myDebuff);
        DelDamage.Broadcast(EDamageType::Poison, myDebuff);
        PoisonTime = myDebuff.DebuffMaxTime;
        if (!PoisonFlag)
        {
          PoisonTickDmg = myDebuff.DamagePerTick;
          PoisonMaxTime = myDebuff.DebuffMaxTime;
          PoisonFlag = true;
          PoisonTick = myDebuff.TickInterval;
          GetWorld()->GetTimerManager().SetTimer(PoisonHandle, this, &UHealthComponent::TimerPoison, PoisonTick, true);
          // UE_LOG(HealthLog, Display, TEXT("Begin Poison"));
        }
      }
    }
  }
}