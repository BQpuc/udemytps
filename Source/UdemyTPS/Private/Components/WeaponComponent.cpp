// https://m.vk.com/id357239644

#include "Components/WeaponComponent.h"
#include "Weapon/TPSWeapon.h"
#include "Engine/World.h"
#include "GameFramework/Character.h"
#include "Player/Animations/EquipFinishedAnimNotify.h"
#include "Player/Animations/FinishReloadNotify.h"
#include "Kismet/GameplayStatics.h"
#include "Develop/TPSGameInstance.h"
#include "Sound/SoundMix.h"

DEFINE_LOG_CATEGORY_STATIC(WeaponComponentLog, All, All)

UWeaponComponent::UWeaponComponent()
{

  PrimaryComponentTick.bCanEverTick = false;
}

void UWeaponComponent::BeginPlay()
{
  Super::BeginPlay();

  // checkf(WeaponData.Num() == 2, TEXT("Character can 2 weapons"));

  CurrentWeaponIndex = 0;
  SpawnWeapons();
  EquipWeapon(CurrentWeaponIndex);
  InitAnimations();
  if (GetOwner())
    GameInstance = GetOwner()->GetGameInstance<UTPSGameInstance>();
}

void UWeaponComponent::EndPlay(const EEndPlayReason::Type EndPlayReason)
{
  CurrentWeapon = nullptr;
  for (auto Weapon : Weapons)
  {
    Weapon->DetachFromActor(FDetachmentTransformRules::KeepWorldTransform);
    Weapon->Destroy();
  }
  Weapons.Empty();
  Super::EndPlay(EndPlayReason);
}

bool UWeaponComponent::IsAmmoEmpty() const
{
  return GetAmmoData().Bullets == 0 && GetAmmoData().Clips == 0;
}

void UWeaponComponent::PlayAnimMontage(UAnimMontage* Animation)
{
  // UE_LOG(WeaponComponentLog, Display, TEXT("PlayAnimMontage"));
  ACharacter* Character = Cast<ACharacter>(GetOwner());
  if (!Character)
  {
    return;
  }
  Character->PlayAnimMontage(Animation);
}

void UWeaponComponent::SpawnWeapons()
{
  // UE_LOG(WeaponComponentLog, Display, TEXT("SpawnWeapons"));
  ACharacter* Character = Cast<ACharacter>(GetOwner());
  if (!GetWorld() || !Character)
  {
    return;
  }
  for (auto OneWeaponData : WeaponData)
  {
    auto Weapon = GetWorld()->SpawnActor<ATPSWeapon>(OneWeaponData.WeaponClass);
    if (!Weapon)
      continue;
    Weapon->OnClipEmpty.AddUObject(this, &UWeaponComponent::OnEmptyClip);
    Weapon->SetOwner(Character);
    Weapons.Add(Weapon);
    AttachWeaponToSocket(Weapon, Character->GetMesh(), WeaponArmorySocketName);
  }
}

bool UWeaponComponent::CanFire() const
{
  // UE_LOG(WeaponComponentLog, Display, TEXT("CanFire"));
  return CurrentWeapon && !EquipAnimInProgress && !ReloadAnimInProgress;
}

bool UWeaponComponent::CanEquip() const
{
  // UE_LOG(WeaponComponentLog, Display, TEXT("CanEquip"));
  return !EquipAnimInProgress && !ReloadAnimInProgress;
}

bool UWeaponComponent::CanReload() const
{
  // UE_LOG(WeaponComponentLog, Display, TEXT("CanReload"));
  return CurrentWeapon                  //
         && !EquipAnimInProgress        //
         && !ReloadAnimInProgress       //
         && CurrentWeapon->CanReload(); //
}

void UWeaponComponent::AttachWeaponToSocket(ATPSWeapon* Weapon, USceneComponent* Mesh, const FName& SocketName)
{
  // UE_LOG(WeaponComponentLog, Display, TEXT("AttachWeaponToSocket"));
  if (!Weapon || !Mesh)
  {
    return;
  }
  const FAttachmentTransformRules AttachmentRules(EAttachmentRule::SnapToTarget, false);
  Weapon->AttachToComponent(Mesh, AttachmentRules, SocketName);
}

void UWeaponComponent::EquipWeapon(int32 WeaponIndex)
{
  // UE_LOG(WeaponComponentLog, Display, TEXT("EquipWeapon"));
  if (WeaponIndex < 0 || WeaponIndex >= Weapons.Num())
  {
    UE_LOG(WeaponComponentLog, Warning, TEXT("Weapon Index not valid!"));
    return;
  }

  ACharacter* Character = Cast<ACharacter>(GetOwner());
  if (!Character)
    return;

  if (CurrentWeapon)
  {
    CurrentWeapon->StopFire();
    CurrentWeapon->Zoom(false);
    AttachWeaponToSocket(CurrentWeapon, Character->GetMesh(), WeaponArmorySocketName);
  }
  CurrentWeapon = Weapons[WeaponIndex];
  // CurrentReloadAnimMontage = WeaponData[WeaponIndex].ReloadAnimMontage;
  const auto CurrentWeaponData = WeaponData.FindByPredicate([&](const FWeaponData& Data) { //
    return Data.WeaponClass == CurrentWeapon->GetClass();
  }); //
  CurrentReloadAnimMontage = CurrentWeaponData ? CurrentWeaponData->ReloadAnimMontage : nullptr;

  AttachWeaponToSocket(CurrentWeapon, Character->GetMesh(), WeaponEquipSocketName);
  EquipAnimInProgress = true;
  PlayAnimMontage(EquipAnimMontage);
}

bool UWeaponComponent::NeedAmmo(TSubclassOf<ATPSWeapon> WeaponClass)
{
  for (const auto Weapon : Weapons)
  {
    if (Weapon && Weapon->IsA(WeaponClass))
    {
      return Weapon->IsAmmoEmpty();
    }
  }
  return false;
}

void UWeaponComponent::Zoom(bool Enabled) 
{
  if (CurrentWeapon)
    CurrentWeapon->Zoom(Enabled);
}

void UWeaponComponent::InitAnimations()
{
  // UE_LOG(WeaponComponentLog, Display, TEXT("InitAnimations"));
  auto EquipFinishedNotify = UTypes::FindNotifyByClass<UEquipFinishedAnimNotify>(EquipAnimMontage);
  if (EquipFinishedNotify)
  {
    EquipFinishedNotify->OnNotified.AddUObject(this, &UWeaponComponent::OnEquipFinished);
  }
  else
  {
    UE_LOG(WeaponComponentLog, Error, TEXT("Need add Notify - EquipFinishedAnimNotify - in Equip montage"));
    checkNoEntry();
  }
  for (auto OneWeaponData : WeaponData)
  {
    auto ReloadFinishedNotify = UTypes::FindNotifyByClass<UFinishReloadNotify>(OneWeaponData.ReloadAnimMontage);
    if (!ReloadFinishedNotify)
    {
      UE_LOG(WeaponComponentLog, Error, TEXT("Need add Notify - FinishReloadNotify - in Reload montage"));
      checkNoEntry();
    }
    ReloadFinishedNotify->OnNotified.AddUObject(this, &UWeaponComponent::OnReloadFinished);
  }
}

void UWeaponComponent::OnEquipFinished(USkeletalMeshComponent* MeshComp)
{
  // UE_LOG(WeaponComponentLog, Display, TEXT("OnEquipFinished"));
  ACharacter* Character = Cast<ACharacter>(GetOwner());
  if (!Character)
  {
    return;
  }
  if (Character->GetMesh() == MeshComp)
  {
    EquipAnimInProgress = false;
  }
}

void UWeaponComponent::OnReloadFinished(USkeletalMeshComponent* MeshComp)
{
  // UE_LOG(WeaponComponentLog, Display, TEXT("OnReloadFinished"));
  ACharacter* Character = Cast<ACharacter>(GetOwner());
  if (!Character)
  {
    return;
  }
  if (Character->GetMesh() == MeshComp)
  {
    ReloadAnimInProgress = false;
  }
}

void UWeaponComponent::OnEmptyClip(ATPSWeapon* Weapon)
{
  if (!Weapon)
    return;
  if (CurrentWeapon == Weapon)
    ChangeClip();
  else
  {
    for (const auto myWeapon : Weapons)
    {
      if (myWeapon == Weapon)
        myWeapon->ChangeClip();
    }
  }
}

void UWeaponComponent::ChangeClip()
{
  if (!CanReload())
    return;
  CurrentWeapon->StopFire();
  CurrentWeapon->ChangeClip();
  ReloadAnimInProgress = true;
  PlayAnimMontage(CurrentReloadAnimMontage);
}

void UWeaponComponent::StartFire()
{
  // UE_LOG(WeaponComponentLog, Display, TEXT("StartFire"));
  if (!CanFire())
  {
    return;
  }
  CurrentWeapon->StartFire();
}

void UWeaponComponent::StopFire()
{
  // UE_LOG(WeaponComponentLog, Display, TEXT("StopFire"));
  CurrentWeapon->StopFire();
}

void UWeaponComponent::NextWeapon()
{
  // UE_LOG(WeaponComponentLog, Display, TEXT("NextWeapon"));
  if (!CanEquip())
  {
    return;
  }
  CurrentWeaponIndex = (CurrentWeaponIndex + 1) % Weapons.Num();
  EquipWeapon(CurrentWeaponIndex);
}

void UWeaponComponent::Reload()
{
  // UE_LOG(WeaponComponentLog, Display, TEXT("Reload"));
  ChangeClip();
}

bool UWeaponComponent::GetWeaponUIData(FWeaponUIData& UIData) const
{
  if (CurrentWeapon)
  {
    UIData = CurrentWeapon->GetUIData();
    return true;
  }
  return false;
}

FAmmoData UWeaponComponent::GetAmmoData() const
{
  return CurrentWeapon->GetAmmoData();
}

bool UWeaponComponent::TryToAddAmmo(TSubclassOf<ATPSWeapon> WeaponType, int32 ClipsAmmo)
{
  for (const auto Weapon : Weapons)
  {
    if (Weapon && Weapon->IsA(WeaponType))
    {
      return Weapon->TryToAddAmmo(ClipsAmmo);
    }
  }
  return false;
}

void UWeaponComponent::ToDefaultSMix()
{
  if (!GameInstance || !GameInstance->ExploseSMix)
    return;
  
  UGameplayStatics::ClearSoundMixModifiers(GetWorld());
  if (ExploseSoundHandle.IsValid())
    GetWorld()->GetTimerManager().ClearTimer(ExploseSoundHandle);
}

void UWeaponComponent::ExploseSound()
{
  if (!GameInstance || !GameInstance->ExploseSMix)
    return;
  UGameplayStatics::PushSoundMixModifier(GetWorld(), GameInstance->ExploseSMix);
  GetWorld()->GetTimerManager().SetTimer(ExploseSoundHandle, this, //
      &UWeaponComponent::ToDefaultSMix, 0.1f, true, DelayToDefSMix);
}