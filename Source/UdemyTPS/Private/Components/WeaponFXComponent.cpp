// https://m.vk.com/id357239644

#include "Components/WeaponFXComponent.h"
#include "NiagaraFunctionLibrary.h"
#include "PhysicalMaterials/PhysicalMaterial.h"
#include "Kismet/GameplayStatics.h"
#include "Components/DecalComponent.h"
#include "Sound/SoundCue.h"


UWeaponFXComponent::UWeaponFXComponent()
{

  PrimaryComponentTick.bCanEverTick = false;
}

void UWeaponFXComponent::BeginPlay()
{
  Super::BeginPlay();
}



void UWeaponFXComponent::PlayImpactFX(const FHitResult& Hit)
{
  FImpactData Effect = DefaultImpactData;
  if (Hit.PhysMaterial.IsValid())
  {
    const UPhysicalMaterial* PhysMat = Hit.PhysMaterial.Get();
    if (ImpactDataMap.Contains(PhysMat))
      Effect = ImpactDataMap[PhysMat];
  }
  UNiagaraFunctionLibrary::SpawnSystemAtLocation(GetWorld(), // Spawn Niagara
      Effect.NiagaraEffect,                                  //
      Hit.ImpactPoint,                                       //
      Hit.ImpactNormal.Rotation());

  UDecalComponent* DecalComponent = UGameplayStatics::SpawnDecalAtLocation( //
      GetWorld(),                                                           //
      Effect.DecalData.Material,                                            //
      Effect.DecalData.Size,                                                //
      Hit.ImpactPoint,                                                      //
      Hit.ImpactNormal.Rotation());
  if (DecalComponent)
    DecalComponent->SetFadeOut(Effect.DecalData.LifeTime, Effect.DecalData.FadeOutTime);
  if (Effect.Sound)
    UGameplayStatics::PlaySoundAtLocation(GetWorld(), Effect.Sound, Hit.ImpactPoint);
}
