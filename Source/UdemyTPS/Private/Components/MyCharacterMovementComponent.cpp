// https://m.vk.com/id357239644


#include "Components/MyCharacterMovementComponent.h"
#include "Player/TPSCharacter.h"
#include "PhysicalMaterials/PhysicalMaterial.h"
#include "Kismet/GameplayStatics.h"
#include "Sound/SoundCue.h"
#include "Sound/SoundAttenuation.h"

float UMyCharacterMovementComponent::GetMaxSpeed() const 
{
  const float MaxSpeed = Super::GetMaxSpeed();
  ATPSCharacter* Player = Cast<ATPSCharacter>(GetPawnOwner());
  if (Player)
  {
      if (Player->CanSprint())
      {
          return MaxSpeed * MaxSpeedRun;
      }
      else
      {
          return MaxSpeed;
      }
  }
  return MaxSpeed; //Player && Player->CanSprint() ? MaxSpeed * MaxSpeedRun : MaxSpeed;
}

void UMyCharacterMovementComponent::PlayStepSound(const FHitResult& Hit) 
{
  if (!StepAttenuation || !GetPawnOwner() || !DefSound)
    return;
  USoundCue* mySound = DefSound;
  if (Hit.PhysMaterial.IsValid())
  {
    const UPhysicalMaterial* PhysMat = Hit.PhysMaterial.Get();
    if (FootSound.Contains(PhysMat))
      mySound = FootSound[PhysMat];
  }
  
  UGameplayStatics::PlaySoundAtLocation(GetWorld(), mySound, GetPawnOwner()->GetActorLocation(), 1.0f, 1.0f, 0.0f, StepAttenuation);
}

void UMyCharacterMovementComponent::BeginPlay()
{
  Super::BeginPlay();


}
