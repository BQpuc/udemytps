// https://m.vk.com/id357239644

#include "Components/TPSAIPerceptionComponent.h"
#include "AIController.h"
#include "Components/HealthComponent.h"
#include "Perception/AISense_Sight.h"
#include "Perception/AISense_Damage.h"

DEFINE_LOG_CATEGORY_STATIC(PerceptionLog, All, All);

AActor* UTPSAIPerceptionComponent::GetClosestEnemy() const
{
  TArray<AActor*> PerciveActors;
  GetCurrentlyPerceivedActors(UAISense_Sight::StaticClass(), PerciveActors); // �������� ���� ������� � ���� ������
  if (PerciveActors.Num() == 0)
  {
    GetCurrentlyPerceivedActors(UAISense_Damage::StaticClass(), PerciveActors); // �������� ������� �������� ����
    if (PerciveActors.Num() == 0)
      return nullptr;
  }

  AAIController* Controller = Cast<AAIController>(GetOwner()); // �������� ���������� �� ���������
  if (!Controller)
    return nullptr;

  APawn* Pawn = Controller->GetPawn(); // �������� Pawn �� �����������
  if (!Pawn)
    return nullptr;

  float BestDistance = MAX_FLT; // ������������ ����� ���� float
  AActor* NearestPawn = nullptr;
  for (AActor* PercActor : PerciveActors)
  {
    UHealthComponent* HealthComp = UTypes::GetPlayerComponent<UHealthComponent>(PercActor);

    APawn* PercivePawn = Cast<APawn>(PercActor); // �������� �� ���� ��
    const auto AreEnemies = PercivePawn && UTypes::AreEnemies(Controller, PercivePawn->Controller);
    // UE_LOG(PerceptionLog, Display, TEXT("Pawn: %s, %i"), *PercivePawn->GetName(), AreEnemies);
    if (HealthComp && !HealthComp->GetIsDead() && AreEnemies)
    {
      float CurrentDistance = (PercActor->GetActorLocation() - Pawn->GetActorLocation()).Size();
      if (CurrentDistance < BestDistance)
      {
        BestDistance = CurrentDistance;
        NearestPawn = PercActor;
      }
    }
  }
  return NearestPawn;
}