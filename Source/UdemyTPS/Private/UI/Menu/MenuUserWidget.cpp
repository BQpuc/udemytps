

#include "UI/Menu/MenuUserWidget.h"
#include "Components/Button.h"
#include "Kismet/GameplayStatics.h"
#include "Develop/TPSGameInstance.h"
#include "Kismet/KismetSystemLibrary.h"
#include "Components/HorizontalBox.h"
#include "Menu/LevelItemWidget.h"
#include "Sound/SoundCue.h"

DEFINE_LOG_CATEGORY_STATIC(MenuWidgetLog, All, All);

void UMenuUserWidget::NativeOnInitialized()
{
  Super::NativeOnInitialized();

  if (StartGameButton)
  {
    StartGameButton->OnClicked.AddDynamic(this, &UMenuUserWidget::OnStartGame);
  }
  if (QuitGameButton)
  {
    QuitGameButton->OnClicked.AddDynamic(this, &UMenuUserWidget::OnQuitGame);
  }
  InitLevelItems();
}

void UMenuUserWidget::OnAnimationFinished_Implementation(const UWidgetAnimation* Animation)
{
  if (Animation != HideAnimation)
    return;
  const auto myGameInstance = GetGameInstance();
  if (!myGameInstance)
    return;
  // ��������� ������� � ��������� ��������
  UGameplayStatics::OpenLevel(this, myGameInstance->GetStartupLvl().LevelName);
}

void UMenuUserWidget::OnStartGame()
{
  PlayAnimation(HideAnimation);
  if (GetWorld() && StartGameSound)
    UGameplayStatics::PlaySound2D(GetWorld(), StartGameSound);
}

void UMenuUserWidget::OnQuitGame()
{
  UKismetSystemLibrary::QuitGame(this, GetOwningPlayer(), EQuitPreference::Quit, true);
}

void UMenuUserWidget::InitLevelItems()
{
  const auto GameInstance = GetGameInstance();
  if (!GameInstance)
    return;
  // ��������� ��� � ���� ������ ���� �� �� ������ ������
  checkf(GameInstance->GetLevelsData().Num() != 0, TEXT("Massiv ypoBHei He DoL>|<en 6b|Tb ||yCTb|M"));

  if (!LevelItemsBox)
    return;
  LevelItemsBox->ClearChildren(); // ������� �������� ���������

  // �������� ����, � ������� ��������� �� ���� ��������� �������
  for (auto LevelData : GameInstance->GetLevelsData())
  {
    // C������ ������ ������ � ���������� � ����������
    const auto LevelItemWidget = CreateWidget<ULevelItemWidget>(GetWorld(), LevelItemWidgetClass);
    if (!LevelItemWidget)
      continue;
    LevelItemWidget->SetLevelData(LevelData); // ���������� ������� ������� ������� � ������
    // ������������� �� �������
    LevelItemWidget->OnLevelSelected.AddUObject(this, &UMenuUserWidget::OnLevelSelected);
    // ������� ��������� �� ������ ��������� � HorizontalBox
    LevelItemsBox->AddChild(LevelItemWidget);
    // ��������� ��������� �� ������ � ������ ��������
    LevelItemWidgets.Add(LevelItemWidget);
  } // ���� ��������� ������� �� ����������
  if (GameInstance->GetStartupLvl().LevelName.IsNone())
  { // ��������� ������ ������� � �������
    OnLevelSelected(GameInstance->GetLevelsData()[0]);
  }
  else // ���� ��������� ������� ���������� (����� �������� �� �������� ������)
  {    // ��������� �������, ������� ������� � ������� ���
    OnLevelSelected(GameInstance->GetStartupLvl());
  }
}

void UMenuUserWidget::OnLevelSelected(const FLevelData& Data)
{
  const auto GameInstance = GetGameInstance();
  if (!GameInstance)
    return; // ����� ������� ������ ������ �������� ���������� ������
  GameInstance->SetStartupLvl(Data);
  // ���������� �� ����� ������� ��������
  for (auto LvlWidget : LevelItemWidgets)
  {
    if (LvlWidget)
    { // ���������� ����� ������ ������
      const auto IsSelected = Data.LevelName == LvlWidget->GetLevelData().LevelName;
      //������������� ��������� �����
      LvlWidget->SetSelected(IsSelected);
    }
  }
}

UTPSGameInstance* UMenuUserWidget::GetGameInstance() const
{
  if (!GetWorld())
    return nullptr;
  return GetWorld()->GetGameInstance<UTPSGameInstance>();
}
