


#include "UI/Menu/MenuHUD.h"
#include "Blueprint/UserWidget.h"
#include "UI/BaseWidget.h"

void AMenuHUD::BeginPlay() 
{
  Super::BeginPlay();

  if (MenuWidgetClass)
  {
    const auto MenuWidget = CreateWidget<UBaseWidget>(GetWorld(), MenuWidgetClass);
    if (MenuWidget)
    {
      MenuWidget->AddToViewport();
      MenuWidget->Show();
    }
  }
}