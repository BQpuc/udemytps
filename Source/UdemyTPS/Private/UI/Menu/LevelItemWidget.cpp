

#include "UI/Menu/LevelItemWidget.h"
#include "Components/Button.h"
#include "Components/TextBlock.h"
#include "Components/Image.h"

DEFINE_LOG_CATEGORY_STATIC(ItemWidgettLog, All, All);

void ULevelItemWidget::NativeOnInitialized()
{
  Super::NativeOnInitialized();

  if (LevelSelectButton)
  { // ������ ������� ������ �� �������
    LevelSelectButton->OnClicked.AddDynamic(this, &ULevelItemWidget::OnLevelItemClicked);
    LevelSelectButton->OnHovered.AddDynamic(this, &ULevelItemWidget::OnLevelItemHovered);
    LevelSelectButton->OnUnhovered.AddDynamic(this, &ULevelItemWidget::OnLevelItemUnhovered);
  }
  UE_LOG(ItemWidgettLog, Warning, TEXT("NativeOnInitialized"));
}

void ULevelItemWidget::SetLevelData(const FLevelData& Data)
{
  LevelData = Data;
  if (LevelNameTextBlock)
  { // ��������� �������� � ��������� �����
    LevelNameTextBlock->SetText(FText::FromName(Data.LevelDisplayName));
  }
  if (LevelImage)
  { // ������������� ����������� ������
    LevelImage->SetBrushFromTexture(Data.LevelThumb);
  }
}

void ULevelItemWidget::SetSelected(bool IsSelected)
{ // ������������� ��������� �����
  if (FrameImage)
  {
    FrameImage->SetVisibility(IsSelected ? ESlateVisibility::Visible : ESlateVisibility::Hidden);
  }
  /*
    if(LevelImage)
    {
      LevelImage->SetColorAndOpacity(IsSelected ? FLinearColor::Red : FLinearColor::White);
    }
  */
}



void ULevelItemWidget::OnLevelItemClicked()
{ 
  OnLevelSelected.Broadcast(LevelData); // �������� ���� ������� ��� ������ ��������
  UE_LOG(ItemWidgettLog, Display, TEXT("Button Clicked %s"), *LevelData.LevelName.ToString());
}

void ULevelItemWidget::OnLevelItemHovered() 
{
  /*
  if (FrameImage)
  {
    FrameImage->SetVisibility(ESlateVisibility::Visible);
  }
  */
}

void ULevelItemWidget::OnLevelItemUnhovered() 
{
  /*
  if (FrameImage)
  {
  FrameImage->SetVisibility(ESlateVisibility::Hidden);
  }
  */
}
