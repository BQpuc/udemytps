


#include "UI/Menu/GoToMenuWidget.h"
#include "Components/Button.h"
#include "Develop/TPSGameInstance.h"
#include "Kismet/GameplayStatics.h"

DEFINE_LOG_CATEGORY_STATIC(GToMenuWidgetLog, All, All);

void UGoToMenuWidget::NativeOnInitialized() 
{
  Super::NativeOnInitialized();

  if (GoToMenuButton)
  {
    GoToMenuButton->OnClicked.AddDynamic(this, &UGoToMenuWidget::OnGoToMenu);
  }
}

void UGoToMenuWidget::OnGoToMenu() 
{
  if (!GetWorld())
    return;
  const auto GameInstance = GetWorld()->GetGameInstance<UTPSGameInstance>();
  if (!GameInstance)
    return;
  if (GameInstance->GetMenuLvlName().IsNone())
  {
    UE_LOG(GToMenuWidgetLog, Error, TEXT("MenuLvlMap in GameInstance is none! "));
    return;
  }
  UGameplayStatics::OpenLevel(this, GameInstance->GetMenuLvlName());
}
