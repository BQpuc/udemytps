


#include "UI/PlayerStateRowWidget.h"
#include "Components/TextBlock.h"
#include "Components/Image.h"

void UPlayerStateRowWidget::SetPlayerName(FText& Text) 
{
  if (PlayerNameBlock)
  {
    PlayerNameBlock->SetText(Text);
  }
}

void UPlayerStateRowWidget::SetKills(FText& Text) 
{
  if (KillsTextBlock)
  {
    KillsTextBlock->SetText(Text);
  }
}

void UPlayerStateRowWidget::SetDeath(FText& Text) 
{
  if (DeathTextBlock)
  {
    DeathTextBlock->SetText(Text);
  }
}

void UPlayerStateRowWidget::SetTeam(FText& Text) 
{
  if (TeamTextBlock)
  {
    TeamTextBlock->SetText(Text);
  }
}

void UPlayerStateRowWidget::SetPlayerIndicatorVisibility(bool Visible) 
{
  if (PlayerIndicatorImage)
  {
    PlayerIndicatorImage->SetVisibility(Visible ? ESlateVisibility::Visible : ESlateVisibility::Hidden);
  }
}

void UPlayerStateRowWidget::SetTeamColor(const FLinearColor& Color) 
{
  if (!TeamImage)
    return;
  TeamImage->SetColorAndOpacity(Color);
}
