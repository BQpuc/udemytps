// https://m.vk.com/id357239644


#include "UI/PauseWidget.h"
#include "Gameframework/GameModeBase.h"
#include "Components/Button.h"
#include "Develop/Sound/SoundFuncLib.h"
#include "Sound/SoundClass.h"

bool UPauseWidget::Initialize() 
{
  bool InitStatus = Super::Initialize(); // �������� �������� � ������, ����� ������ �� ���� �������
  if (ClearPauseButton) // ������������� �� ������� ������� ������ ��� �����
  {
    ClearPauseButton->OnClicked.AddDynamic(this, &UPauseWidget::OnClearPause);
  }
  if (SoundButton)
  {
    SoundButton->OnClicked.AddDynamic(this, &UPauseWidget::OnApplaySound);
  }
    
  return InitStatus; 
}

void UPauseWidget::OnClearPause() 
{
  if (!GetWorld() || !GetWorld()->GetAuthGameMode()) 
    return;
  GetWorld()->GetAuthGameMode()->ClearPause(); // ���������� ���� ���� �������
}

void UPauseWidget::OnApplaySound() 
{
  if (CharacterSound.SoundClass)
    USoundFuncLib::SetSoundClassVolume(CharacterSound.SoundClass, CharacterSound.Volume);
  if (ImpactSound.SoundClass)
    USoundFuncLib::SetSoundClassVolume(ImpactSound.SoundClass, ImpactSound.Volume);
  if (MusicSound.SoundClass)
    USoundFuncLib::SetSoundClassVolume(MusicSound.SoundClass, MusicSound.Volume);
  if (WeaponSound.SoundClass)
    USoundFuncLib::SetSoundClassVolume(WeaponSound.SoundClass, WeaponSound.Volume);
}
