// https://m.vk.com/id357239644


#include "UI/GameDataWidget.h"
#include "TPSGameModeBase.h"
#include "Player/TPSPlayerState.h"



int32 UGameDataWidget::GetCurrentRoundNum() const
{
  ATPSGameModeBase* GameMode = GetTPSGameMode();
  return GameMode ? GameMode->GetCurrentRound() : 0;
}

int32 UGameDataWidget::GetTotalRoundsNum() const
{
  ATPSGameModeBase* GameMode = GetTPSGameMode();
  return GameMode ? GameMode->GetGameData().RoundsNum : 0;
}

int32 UGameDataWidget::GetRoundSecondsRemaining() const
{
  ATPSGameModeBase* GameMode = GetTPSGameMode();
  return GameMode ? GameMode->GetRoundSecondsRemaning() : 0;
}

ATPSGameModeBase* UGameDataWidget::GetTPSGameMode() const
{
  return GetWorld() ? Cast<ATPSGameModeBase>(GetWorld()->GetAuthGameMode()) : nullptr;
}

ATPSPlayerState* UGameDataWidget::GetTPSPlayerState() const
{
  return GetOwningPlayer() ? Cast<ATPSPlayerState>(GetOwningPlayer()->PlayerState) : nullptr;
}
