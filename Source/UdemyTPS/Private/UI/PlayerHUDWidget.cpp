// https://m.vk.com/id357239644

#include "UI/PlayerHUDWidget.h"
#include "Components/HealthComponent.h"
#include "Components/WeaponComponent.h"
#include "Player/TPSPlayerState.h"
#include "Components/ProgressBar.h"


float UPlayerHUDWidget::GetHealthPercent() const
{
  UHealthComponent* HealthComp = UTypes::GetPlayerComponent<UHealthComponent>(GetOwningPlayerPawn());
  if (HealthComp)
  {
    return HealthComp->GetHealthPercent();
  }
  return 0.0f;
}

bool UPlayerHUDWidget::GetWeaponUIData(FWeaponUIData& UIData) const
{
  UWeaponComponent* WeaponComp = UTypes::GetPlayerComponent<UWeaponComponent>(GetOwningPlayerPawn());
  if (WeaponComp)
  {
    WeaponComp->GetWeaponUIData(UIData);
    return true;
  }
  return false;
}

FAmmoData UPlayerHUDWidget::GetAmmoData() const
{
  UWeaponComponent* WeaponComp = UTypes::GetPlayerComponent<UWeaponComponent>(GetOwningPlayerPawn());
  if (WeaponComp)
  {
    return WeaponComp->GetAmmoData();
  }
  return FAmmoData{0, 0, false};
}

bool UPlayerHUDWidget::IsPlayerAlive() const
{
  UHealthComponent* HealthComp = UTypes::GetPlayerComponent<UHealthComponent>(GetOwningPlayerPawn());
  return HealthComp && !HealthComp->GetIsDead();
}

bool UPlayerHUDWidget::IsPlayerSpectating() const
{
  APlayerController* controller = GetOwningPlayer();
  return controller && controller->GetStateName() == NAME_Spectating;
}

bool UPlayerHUDWidget::Initialize()
{
  if (GetOwningPlayer()) //��������� �� ������� �� ��������� �� ����������
  {
    GetOwningPlayer()->GetOnNewPawnNotifier().AddUObject(this, &UPlayerHUDWidget::OnNewPawn); // �������� ������ �� ������� � ������ �������
    OnNewPawn(GetOwningPlayerPawn());
  }
  /*UHealthComponent* HealthComp = UTypes::GetPlayerComponent<UHealthComponent>(GetOwningPlayerPawn());
  if (HealthComp)
  {
    HealthComp->OnChangeHealth.AddUObject(this, &UPlayerHUDWidget::OnHealthChanged);
  }*/
  return Super::Initialize();
}

void UPlayerHUDWidget::OnHealthChanged(float Health, float Damage)
{
  if (Damage > 0)
  {
      OnTakeDamage();
    if (!IsAnimationPlaying(DamageAnimation)) // �������� �� ������������ ��������
    { // ������ ��������
      PlayAnimation(DamageAnimation);
    }
  }
  UpdateHealthBar();
}

void UPlayerHUDWidget::OnNewPawn(APawn* NewPawn) 
{
  UHealthComponent* HealthComp = UTypes::GetPlayerComponent<UHealthComponent>(NewPawn);
  if (!HealthComp)
    return;
  if (!HealthComp->OnChangeHealth.IsBoundToObject(this))
  {
    HealthComp->OnChangeHealth.AddUObject(this, &UPlayerHUDWidget::OnHealthChanged);
  }
  UpdateHealthBar();
}

void UPlayerHUDWidget::UpdateHealthBar() 
{
  if (HealthProgressBar)
  {
    HealthProgressBar->SetFillColorAndOpacity(GetHealthPercent() > PercentColor ? GoodColor : BadColor);
  }
}

int32 UPlayerHUDWidget::GetKillsNum() const
{
  ATPSPlayerState* PlayerState = GetTPSPlayerState();
  return PlayerState ? PlayerState->GetKillsNum() : 0;
}

FString UPlayerHUDWidget::FormatBullets(int32 Bullets) const
{
  const int32 MaxLen = 2; // �������� ����������� ����� ����� ���������
  const TCHAR PrefixSimbol = '0'; // ����������� ������
  auto BulletsStr = FString::FromInt(Bullets); // ��������������� ���� � FString
  const auto SymbolsNumToAdd = MaxLen - BulletsStr.Len(); // ���-�� ����� ������� ����� �������� ����� ��������
  if (SymbolsNumToAdd > 0) // ����� �� ��������� ����
  { // ������� ������ �� ���������� ��������
    BulletsStr = FString::ChrN(SymbolsNumToAdd, PrefixSimbol).Append(BulletsStr);
  }
  
  return BulletsStr;
}

ATPSPlayerState* UPlayerHUDWidget::GetTPSPlayerState() const
{
  return GetOwningPlayer() ? Cast<ATPSPlayerState>(GetOwningPlayer()->PlayerState) : nullptr;
}
