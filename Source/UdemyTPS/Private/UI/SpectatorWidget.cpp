// https://m.vk.com/id357239644


#include "UI/SpectatorWidget.h"
#include "Components/RespawnComponent.h"

bool USpectatorWidget::GetRespawnTime(int32& CountDownTime) const
{
  URespawnComponent* RespComponent = UTypes::GetPlayerComponent<URespawnComponent>(GetOwningPlayer());
  if (!RespComponent || !RespComponent->IsRespawnInProgress())
    return false;

  CountDownTime = RespComponent->GetRespawnCountDown();
  return true;
}