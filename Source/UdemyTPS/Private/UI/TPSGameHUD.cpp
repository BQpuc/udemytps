// https://m.vk.com/id357239644

#include "UI/TPSGameHUD.h"
#include "Engine/Canvas.h"
#include "UI/PlayerHUDWidget.h"
#include "TPSGameModeBase.h"
#include "UI/BaseWidget.h"

DEFINE_LOG_CATEGORY_STATIC(GameHUDLog, All, All);

void ATPSGameHUD::DrawHUD()
{
  Super::DrawHUD();
  //DrawCross();
}

void ATPSGameHUD::DrawCross()
{
  const TInterval<float> Center(Canvas->SizeX * 0.5f, Canvas->SizeY * 0.5f);

  // StartX, StartY, EndX, EndY, Color, Thikness
  DrawLine(Center.Min - 10.0f, Center.Max, Center.Min + 10.0f, Center.Max, FLinearColor::Black, 2.0f);
  DrawLine(Center.Min, Center.Max - 10.0f, Center.Min, Center.Max + 10.0f, FLinearColor::Black, 2.0f);
}

void ATPSGameHUD::BeginPlay()
{
  Super::BeginPlay();

  GameWidgets.Add(EMatchState::InProgress, CreateWidget<UBaseWidget>(GetWorld(), PlayerWidget));
  GameWidgets.Add(EMatchState::Pause, CreateWidget<UBaseWidget>(GetWorld(), PauseWidget));
  GameWidgets.Add(EMatchState::GameOver, CreateWidget<UBaseWidget>(GetWorld(), GameOverWidget));

  for (auto GameWidgetPair : GameWidgets)
  {
    UUserWidget* GameWidget = GameWidgetPair.Value;
    if (!GameWidget)
      continue;
    GameWidget->AddToViewport();
    GameWidget->SetVisibility(ESlateVisibility::Hidden);
  }

  if (GetWorld())
  {
    ATPSGameModeBase* GameMode = Cast<ATPSGameModeBase>(GetWorld()->GetAuthGameMode());
    if (GameMode)
    {
      GameMode->OnMathStateChanged.AddUObject(this, &ATPSGameHUD::OnMatchStateChahged);
    }
  }
}

void ATPSGameHUD::OnMatchStateChahged(EMatchState State)
{
  if (CurrentWidget) // ������ ��� ������ ������� � ��� �� ����������
  {
    CurrentWidget->SetVisibility(ESlateVisibility::Hidden); // �������� ������� ������
  }
  if (GameWidgets.Contains(State)) // ���������, ���������� �� � ������� ������� � ������ State
  {
    CurrentWidget = GameWidgets[State]; // ������������� �������� �������
  }
  if (CurrentWidget)
  {
    CurrentWidget->SetVisibility(ESlateVisibility::Visible); // ������ ������� ������� ������
    CurrentWidget->Show();
  }
  UE_LOG(GameHUDLog, Display, TEXT("Math State: %s"), *UEnum::GetValueAsString(State));
}