


#include "UI/BaseWidget.h"
#include "Kismet/GameplayStatics.h"
#include "Sound/SoundCue.h"

 void UBaseWidget::Show() 
 {
   if (ShowAnimation)
   {
     PlayAnimation(ShowAnimation);
   }
   if (GetWorld() && OpenSound)
     UGameplayStatics::PlaySound2D(GetWorld(), OpenSound);
 }