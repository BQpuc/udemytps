


#include "UI/GameOverWidget.h"
#include "TPSGameModeBase.h"
#include "Player/TPSPlayerState.h"
#include "UI/PlayerStateRowWidget.h"
#include "Components/VerticalBox.h"
#include "Components/Button.h"
#include "Kismet/GameplayStatics.h"


bool UGameOverWidget::Initialize()
{
  bool Init = Super::Initialize();
  if (GetWorld())
  {
    ATPSGameModeBase* GameMode = Cast<ATPSGameModeBase>(GetWorld()->GetAuthGameMode());
    if (GameMode)
    {
      GameMode->OnMathStateChanged.AddUObject(this, &UGameOverWidget::OnMatchStateChanhed);
    }
  }
  if (ResetLevelButton)
  {
    ResetLevelButton->OnClicked.AddDynamic(this, &UGameOverWidget::OnResetLevel);
  }
  return Init;
}

void UGameOverWidget::OnMatchStateChanhed(EMatchState State) 
{
  if (State == EMatchState::GameOver)
  {
    UpdatePlayerStat();
  }
}

void UGameOverWidget::UpdatePlayerStat() 
{
  if (!GetWorld() || !PlayerStatBox)
    return;
  PlayerStatBox->ClearChildren();

  for (auto It = GetWorld()->GetControllerIterator(); It; ++It)
  {
    const auto Controller = It->Get();
    if (!Controller)
      continue;
    const auto PlayerState = Cast<ATPSPlayerState>(Controller->PlayerState);
    if (!PlayerState)
      continue;
    const auto StatWidget = CreateWidget<UPlayerStateRowWidget>(GetWorld(), PlayerStatRowWidget);
    if (!StatWidget)
      continue;
    FText Name = FText::FromString(PlayerState->GetPlayerName());
    FText Kills = UTypes::TextFromInt(PlayerState->GetKillsNum());
    FText Death = UTypes::TextFromInt(PlayerState->GetDeathNum());
    FText Team = UTypes::TextFromInt(PlayerState->GetTeamID());
    StatWidget->SetPlayerName(Name);
    StatWidget->SetKills(Kills);
    StatWidget->SetDeath(Death);
    StatWidget->SetTeam(Team);
    StatWidget->SetPlayerIndicatorVisibility(Controller->IsPlayerController());
    StatWidget->SetTeamColor(PlayerState->GetTeamColor());

    PlayerStatBox->AddChild(StatWidget);
  }
}

void UGameOverWidget::OnResetLevel() 
{
 //const FName CurrentLevelName = "TestLevel";
  if (CurrentLevelName == "")
    CurrentLevelName = FName(UGameplayStatics::GetCurrentLevelName(this));
  UGameplayStatics::OpenLevel(this, CurrentLevelName);
}
