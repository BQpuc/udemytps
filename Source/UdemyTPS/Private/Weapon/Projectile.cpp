// Shoot Them Up Game, All Rights Reserved.

#include "Weapon/Projectile.h" //#include "Weapon/STUProjectile.h"
#include "Components/SphereComponent.h"
#include "GameFramework/ProjectileMovementComponent.h"
#include "DrawDebugHelpers.h"
#include "Kismet/GameplayStatics.h"
#include "Engine/World.h"
#include "Components/WeaponFXComponent.h"

AProjectile::AProjectile() // ASTUProjectile::ASTUProjectile()
{
  PrimaryActorTick.bCanEverTick = false;

  CollisionComponent = CreateDefaultSubobject<USphereComponent>(TEXT("SphereComponent")); // Create collision
  CollisionComponent->InitSphereRadius(5.0f);                                             // Projectile radius
  CollisionComponent->SetCollisionEnabled(ECollisionEnabled::QueryOnly); // Query Only (No Physics Collision) Collison Turn on
  CollisionComponent->SetCollisionResponseToAllChannels(ECollisionResponse::ECR_Block); // Projectile collision Block now
  CollisionComponent->bReturnMaterialOnMove = true;                                     // Turn on Physical Material in Hit
  SetRootComponent(CollisionComponent);                                                 // Set root

  MovementComponent = CreateDefaultSubobject<UProjectileMovementComponent>(TEXT("MovementComponent"));
  MovementComponent->UpdatedComponent = RootComponent;
  MovementComponent->InitialSpeed = 2000.0f;
  MovementComponent->ProjectileGravityScale = 0.0f;

  WeaponFXComponent = CreateDefaultSubobject<UWeaponFXComponent>("FXComponent");
}

void AProjectile::BeginPlay()
{
  Super::BeginPlay();

  check(MovementComponent);
  check(CollisionComponent);
  check(GetWorld());
  check(WeaponFXComponent);

  MovementComponent->Velocity = ShotDirection * MovementComponent->InitialSpeed;
  CollisionComponent->IgnoreActorWhenMoving(this, true);
  SetLifeSpan(LifeTime);
  CollisionComponent->OnComponentHit.AddDynamic(this, &AProjectile::OnProjectileHit);
}

AController* AProjectile::GetController() const
{
  const auto Pawn = Cast<APawn>(GetOwner());
  return Pawn ? Pawn->GetController() : nullptr;
}

void AProjectile::OnProjectileHit(
    UPrimitiveComponent* HitComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit)
{
  if (!GetWorld())
    return;

  UGameplayStatics::ApplyRadialDamage(GetWorld(), //
      DamageAmount,                               //
      GetActorLocation(),                         //
      DamageRadius,                               //
      DamageType,                                 //
      {GetOwner()},                               //
      this,                                       //
      GetController(), IsFullDamage);

  //UKismetSystemLibrary::DrawDebugSphere(GetWorld(), GetActorLocation(), DamageRadius, 12, FLinearColor::Red, 5.0f, 4.0f);

  MovementComponent->StopMovementImmediately();
  WeaponFXComponent->PlayImpactFX(Hit);

  Destroy();
}

/*

AProjectile::AProjectile()
{
  PrimaryActorTick.bCanEverTick = false;

  SphereCollission = CreateDefaultSubobject<USphereComponent>("SphereCollision");
  SphereCollission->InitSphereRadius(5.0f);
  RootComponent = SphereCollission;

  MovementComponent = CreateDefaultSubobject<UProjectileMovementComponent>("Projectile");

  MovementComponent->InitialSpeed = 2000.0f;
  MovementComponent->ProjectileGravityScale = 0.0f;

}

void AProjectile::BeginPlay()
{
  Super::BeginPlay();
  check(MovementComponent);
  MovementComponent->Velocity = ShotDirection * 2000.0f;
  SetLifeSpan(5.0f);

}

void AProjectile::SetShotDirection(const FVector& Diraction)
{ ShotDirection = Diraction; }*/