
// Shoot Them Up Game, All Rights Reserved.

#include "Weapon/RifleWeapon.h"
#include "Engine/World.h"
#include "DrawDebugHelpers.h"
#include "Components/WeaponFXComponent.h"
#include "NiagaraComponent.h"
#include "NiagaraFunctionLibrary.h"
#include "Kismet/GameplayStatics.h"
#include "Sound/SoundCue.h"
#include "Camera/PlayerCameraManager.h"

ARifleWeapon::ARifleWeapon()
{
  WeaponFXComponent = CreateDefaultSubobject<UWeaponFXComponent>("FXComponent");
}

void ARifleWeapon::BeginPlay()
{
  Super::BeginPlay();

  check(WeaponFXComponent);

  const auto Controller = Cast<APlayerController>(GetController());
  if (Controller && Controller->PlayerCameraManager)
    StartFOV = Controller->PlayerCameraManager->GetFOVAngle();
}

void ARifleWeapon::StartFire()
{
  GetWorldTimerManager().SetTimer(ShotTimerHandle, this, &ARifleWeapon::MakeShot, TimeBetweenShots, true);
  MakeShot();
  InitFX();
}

void ARifleWeapon::StopFire()
{
  bool TimerValid = ShotTimerHandle.IsValid();
  if (TimerValid)
    GetWorldTimerManager().ClearTimer(ShotTimerHandle);
  SetFxActive(false);
}

void ARifleWeapon::MakeShot()
{
  if (!GetWorld())
  {
    StopFire();
    return;
  }
  if (IsAmmoEmpty())
  {
    StopFire();
    UGameplayStatics::SpawnSoundAttached(NoAmmoSound, WeaponMesh);
    return;
  }
  FVector TraceStart, TraceEnd;
  if (!GetTraceData(TraceStart, TraceEnd))
  {
    StopFire();
    return;
  }
  FHitResult HitResult;
  MakeHit(HitResult, TraceStart, TraceEnd);
  FVector TraceFXEnd = TraceEnd;
  if (HitResult.bBlockingHit)
  {
    TraceFXEnd = HitResult.ImpactPoint;
    MakeDamage(HitResult);
    WeaponFXComponent->PlayImpactFX(HitResult);
  }
  SpawnTraceFX(GetMuzzleWorldLocation(), TraceFXEnd);
  if (FireSound)
    UGameplayStatics::PlaySoundAtLocation(GetWorld(), FireSound, GetActorLocation());
  DecreaseAmmo();
}

bool ARifleWeapon::GetTraceData(FVector& TraceStart, FVector& TraceEnd) const
{
  FVector ViewLocation;
  FRotator ViewRotation;
  if (!GetPlayerViewPoint(ViewLocation, ViewRotation))
    return false;

  TraceStart = ViewLocation;
  const auto HalfRad = FMath::DegreesToRadians(BulletSpread);
  const FVector ShootDirection = FMath::VRandCone(ViewRotation.Vector(), HalfRad);
  TraceEnd = TraceStart + ShootDirection * TraceMaxDistance;
  return true;
}

void ARifleWeapon::MakeDamage(const FHitResult& HitResult)
{
  const auto DamagedActor = HitResult.GetActor();
  if (!DamagedActor)
    return;
  FPointDamageEvent PointDamageEvent;  
  PointDamageEvent.HitInfo = HitResult;
  DamagedActor->TakeDamage(DamageAmount, PointDamageEvent, GetController(), this);
}

void ARifleWeapon::InitFX()
{
  if (!MuzzleFXComponent)
  {
    MuzzleFXComponent = SpawnMuzzleFX();
  }
  SetFxActive(true);
}

void ARifleWeapon::SetFxActive(bool Visible)
{
  if (MuzzleFXComponent)
  {
    MuzzleFXComponent->SetPaused(!Visible);
    MuzzleFXComponent->SetVisibility(Visible, true);
  }
}

void ARifleWeapon::SpawnTraceFX(const FVector& TraceStart, const FVector& TraceEnd)
{
  UNiagaraComponent* BeamFX = UNiagaraFunctionLibrary::SpawnSystemAtLocation(GetWorld(), //
      TraceFX,                                                                           //
      TraceStart);
  if (BeamFX)
  {
    BeamFX->SetNiagaraVariableVec3(TraceTargetName, TraceEnd);
  }
}

AController* ARifleWeapon::GetController() const
{
  const auto Pawn = Cast<APawn>(GetOwner());
  return Pawn ? Pawn->GetController() : nullptr;
}

void ARifleWeapon::Zoom(bool Enabled) 
{
  const auto Controller = Cast<APlayerController>(GetController());
  if (!Controller || !Controller->PlayerCameraManager)
    return;
  float NewFOV = Enabled ? Controller->PlayerCameraManager->GetFOVAngle() / ZoomModifier : StartFOV;
  Controller->PlayerCameraManager->SetFOV(NewFOV);
}
