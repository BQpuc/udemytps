
#include "Weapon/TPSWeapon.h"
#include "Components/SkeletalMeshComponent.h"
#include "Engine/World.h"
#include "GameFramework/Character.h"
#include "GameFramework/Controller.h"
#include "NiagaraFunctionLibrary.h"
#include "NiagaraComponent.h"
#include "Sound/SoundCue.h"


DEFINE_LOG_CATEGORY_STATIC(WeaponLog, All, All);

ATPSWeapon::ATPSWeapon()
{
  PrimaryActorTick.bCanEverTick = false;

  WeaponMesh = CreateDefaultSubobject<USkeletalMeshComponent>("WeaponMesh");
  SetRootComponent(WeaponMesh);
}

void ATPSWeapon::BeginPlay()
{
  Super::BeginPlay();

  check(WeaponMesh);
  checkf(DefaultAmmo.Bullets > 0, TEXT("Bullets count must be > 0"));
  checkf(DefaultAmmo.Clips > 0, TEXT("Clips count must be > 0"));
  CurrentAmmo = DefaultAmmo;
}

void ATPSWeapon::StartFire() {}

void ATPSWeapon::StopFire() {}

void ATPSWeapon::Zoom(bool Enabled) {}

void ATPSWeapon::MakeShot() {}

/*APlayerController* ATPSWeapon::GetPlayerController() const
{
  const auto Player = Cast<ACharacter>(GetOwner());
  if (!Player)
    return nullptr;

  return Player->GetController<APlayerController>();
}*/

bool ATPSWeapon::GetPlayerViewPoint(FVector& ViewLocation, FRotator& ViewRotation) const
{
  ACharacter* myCharacter = Cast<ACharacter>(GetOwner());
  if (!myCharacter)
    return false;
  if (myCharacter->IsPlayerControlled())
  {
    const auto Controller = myCharacter->GetController<APlayerController>();
    if (!Controller)
      return false;
    Controller->GetPlayerViewPoint(ViewLocation, ViewRotation);
  }
  else
  {
    ViewLocation = GetMuzzleWorldLocation();
    ViewRotation = WeaponMesh->GetSocketRotation(MuzzleSocketName);
  }
  return true;
}

FVector ATPSWeapon::GetMuzzleWorldLocation() const
{
  return WeaponMesh->GetSocketLocation(MuzzleSocketName);
}

bool ATPSWeapon::GetTraceData(FVector& TraceStart, FVector& TraceEnd) const
{
  FVector ViewLocation;
  FRotator ViewRotation;
  if (!GetPlayerViewPoint(ViewLocation, ViewRotation))
    return false;

  TraceStart = ViewLocation;
  const FVector ShootDirection = ViewRotation.Vector();
  TraceEnd = TraceStart + ShootDirection * TraceMaxDistance;
  return true;
}

void ATPSWeapon::MakeHit(FHitResult& HitResult, const FVector& TraceStart, const FVector& TraceEnd)
{
  if (!GetWorld())
    return;

  FCollisionQueryParams CollisionParams;
  CollisionParams.AddIgnoredActor(GetOwner());
  CollisionParams.bReturnPhysicalMaterial = true;
  GetWorld()->LineTraceSingleByChannel(HitResult, TraceStart, TraceEnd, ECollisionChannel::ECC_Visibility, CollisionParams);
}

void ATPSWeapon::DecreaseAmmo()
{
  if (CurrentAmmo.Bullets == 0)
  {
    //UE_LOG(WeaponLog, Display, TEXT("=== clip is empty ==="));
    return;
  }
  CurrentAmmo.Bullets--;
  if (IsClipEmpty() && !IsAmmoEmpty())
  {
    StopFire();
    OnClipEmpty.Broadcast(this);
  }
}

// Return true if Clips and Ammo = 0
bool ATPSWeapon::IsAmmoEmpty() const
{
  return !CurrentAmmo.Infinite && CurrentAmmo.Clips == 0 && CurrentAmmo.Bullets == 0;
}

bool ATPSWeapon::IsClipEmpty() const
{
  return CurrentAmmo.Bullets == 0;
}

// Raload logic no animation
void ATPSWeapon::ChangeClip()
{
  if (!CurrentAmmo.Infinite)
  {

    CurrentAmmo.Clips--;
  }
  CurrentAmmo.Bullets = DefaultAmmo.Bullets;
  //UE_LOG(WeaponLog, Display, TEXT("=== RELOAD ==="));
}

bool ATPSWeapon::CanReload() const
{
  return CurrentAmmo.Bullets < DefaultAmmo.Bullets && CurrentAmmo.Clips > 0;
}

bool ATPSWeapon::TryToAddAmmo(int32 ClipsAmount)
{
  if (CurrentAmmo.Infinite || IsAmmoFull() || ClipsAmount <= 0)
    return false;

  if (IsAmmoEmpty())
  {
    CurrentAmmo.Clips = FMath::Clamp(ClipsAmount, 0, DefaultAmmo.Clips + 1);
    OnClipEmpty.Broadcast(this);
   // UE_LOG(WeaponLog, Display, TEXT("Clips = 0, Bullets = 0"));
  }
  else if (CurrentAmmo.Clips < DefaultAmmo.Clips)
  {
    int32 Clips = CurrentAmmo.Clips + ClipsAmount;
    CurrentAmmo.Clips = FMath::Clamp(Clips, 0, DefaultAmmo.Clips);
    if (DefaultAmmo.Clips < Clips)
      CurrentAmmo.Bullets = DefaultAmmo.Bullets;
   // UE_LOG(WeaponLog, Display, TEXT("Clips < DefaultAmmo.Clips"));
  }
  else
  {
    CurrentAmmo.Bullets = DefaultAmmo.Bullets;
    //UE_LOG(WeaponLog, Display, TEXT("Clips Maximum"));
  }
  return true;
}

void ATPSWeapon::LogAmmo()
{
  FString AmmoInfo = "Ammo: " + FString::FromInt(CurrentAmmo.Bullets);
  AmmoInfo += CurrentAmmo.Infinite ? "Infinete" : FString::FromInt(CurrentAmmo.Clips);
 // UE_LOG(WeaponLog, Display, TEXT("%s"), *AmmoInfo);
}

// Maximum Clips and Bullets
bool ATPSWeapon::IsAmmoFull() const
{
  return CurrentAmmo.Bullets == DefaultAmmo.Bullets && CurrentAmmo.Clips == DefaultAmmo.Clips;
}

UNiagaraComponent* ATPSWeapon::SpawnMuzzleFX()
{
  if (!MuzzleFX)
    return nullptr;
  UNiagaraComponent* FX = UNiagaraFunctionLibrary::SpawnSystemAttached(MuzzleFX, //
      WeaponMesh,                                                                //
      MuzzleSocketName,                                                          //
      FVector::ZeroVector,                                                       //
      FRotator::ZeroRotator,                                                     //
      EAttachLocation::SnapToTarget,                                             //
      true);
  if (FX)
    return FX;
  return nullptr;
}
