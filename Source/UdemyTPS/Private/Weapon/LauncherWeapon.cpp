// Shoot Them Up Game, All Rights Reserved.

#include "Weapon/LauncherWeapon.h"
#include "Weapon/Projectile.h"
#include "Kismet/GameplayStatics.h"
#include "Sound/SoundCue.h"

void ALauncherWeapon::StartFire()
{
  MakeShot();
}

void ALauncherWeapon::MakeShot()
{
  if (!GetWorld())
    return;
  if (IsAmmoEmpty())
  {
    UGameplayStatics::SpawnSoundAtLocation(GetWorld(), NoAmmoSound, GetActorLocation());
    return;
  }
  FVector TraceStart, TraceEnd;
  if (!GetTraceData(TraceStart, TraceEnd))
    return;

  FHitResult HitResult;
  MakeHit(HitResult, TraceStart, TraceEnd);

  const FVector EndPoint = HitResult.bBlockingHit ? HitResult.ImpactPoint : TraceEnd;
  const FVector Direction = (EndPoint - GetMuzzleWorldLocation()).GetSafeNormal();

  const FTransform SpawnTransform(FRotator::ZeroRotator, GetMuzzleWorldLocation());
  AProjectile* Projectile = GetWorld()->SpawnActorDeferred<AProjectile>(ProjectileClass, SpawnTransform);
  if (Projectile)
  {
    Projectile->SetShotDirection(Direction);
    Projectile->DamageRadius = ExploseRadius;
    Projectile->DamageAmount = MaxDamage;
    Projectile->DamageType = DamageType;
    Projectile->IsFullDamage = IsFullDamage;
    Projectile->SetOwner(GetOwner());
    Projectile->FinishSpawning(SpawnTransform);
  }
  DecreaseAmmo();
  SpawnMuzzleFX();
  UGameplayStatics::SpawnSoundAttached(FireSound, WeaponMesh);
}


/* 


DEFINE_LOG_CATEGORY_STATIC(LauncherLog, All, All);

void ALauncherWeapon::StartFire()
{
  Super::StartFire();
  MakeShot();
}

void ALauncherWeapon::MakeShot()
{
  if (!Player)
  {
    UE_LOG(LauncherLog, Warning, TEXT("MakeShot Player not valid"));
    return;
  }
  if (!Controller)
  {
    UE_LOG(LauncherLog, Warning, TEXT("MakeShot Controller not valid"));
    return;
  }

  FVector TraceStart, TraceEnd;
  GetTraceData(TraceStart, TraceEnd);
  FHitResult Hit;
  MakeHit(Hit, TraceStart, TraceEnd);
  FVector EndPoint = Hit.bBlockingHit ? EndPoint = Hit.ImpactPoint : EndPoint = TraceEnd;
 
  const FVector Diraction = (EndPoint - GetMuzzleLocation().GetSafeNormal());
  
  const FTransform SpTransform(FRotator::ZeroRotator, GetMuzzleLocation());//, FVector(1.0, 1.0, 1.0));
  AProjectile* Projectile = GetWorld()->SpawnActorDeferred<AProjectile>(ProjectileClass, SpTransform);
  //AProjectile* Projectile = GetWorld()->SpawnActor<AProjectile>(GetMuzzleLocation(), Diraction.Rotation());
  if (Projectile)
  {
    Projectile->SetShotDirection(Diraction);

    Projectile->FinishSpawning(SpTransform);
    //Projectile->SetLifeSpan(5.0);
  }
}*/