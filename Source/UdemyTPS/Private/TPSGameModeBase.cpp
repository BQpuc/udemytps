// https://m.vk.com/id357239644

#include "TPSGameModeBase.h"
#include "Player/TPSCharacter.h"
#include "Player/TPSPlayerController.h"
#include "UI/TPSGameHUD.h"
#include "AIController.h"
#include "Player/TPSPlayerState.h"
#include "Components/RespawnComponent.h"
#include "EngineUtils.h"
#include "Components/WeaponComponent.h"
#include "Develop/Types.h"

DEFINE_LOG_CATEGORY_STATIC(GameModeLog, All, All)

//  DefaultPawnClass = ADefaultPawn::StaticClass();
//	PlayerControllerClass = APlayerController::StaticClass();
//	PlayerStateClass = APlayerState::StaticClass();
//	GameStateClass = AGameStateBase::StaticClass();
//	HUDClass = AHUD::StaticClass();
//	GameSessionClass = AGameSession::StaticClass();
//	SpectatorClass = ASpectatorPawn::StaticClass();
//	ReplaySpectatorPlayerControllerClass = APlayerController::StaticClass();
//	ServerStatReplicatorClass = AServerStatReplicator::StaticClass();

ATPSGameModeBase::ATPSGameModeBase()
{
  DefaultPawnClass = ATPSCharacter::StaticClass();
  PlayerControllerClass = ATPSPlayerController::StaticClass();
  HUDClass = ATPSGameHUD::StaticClass();
  PlayerStateClass = ATPSPlayerState::StaticClass();
}

void ATPSGameModeBase::StartPlay()
{
  Super::StartPlay();
  SpawnBots();
  CreateTeamsInfo();
  CurrentRound = 1;
  StartRound();
  SetMatchState(EMatchState::InProgress);
}

UClass* ATPSGameModeBase::GetDefaultPawnClassForController_Implementation(AController* InController)
{
  if (InController && InController->IsA<AAIController>())
  {
    return AIPawnClass;
  }
  return Super::GetDefaultPawnClassForController_Implementation(InController);
}

void ATPSGameModeBase::Killed(AController* KillerController, AController* VictimController)
{
  ATPSPlayerState* KillerState = KillerController ? Cast<ATPSPlayerState>(KillerController->PlayerState) : nullptr;
  ATPSPlayerState* VictimState = VictimController ? Cast<ATPSPlayerState>(VictimController->PlayerState) : nullptr;
  UE_LOG(GameModeLog, Display, TEXT("Killer: %s, Victim: %s"), *KillerState->GetName(), *VictimState->GetName());
  if (KillerState)
    KillerState->AddKill();
  if (VictimState)
    VictimState->AddDeath();
  StartRespawn(VictimController);
}

void ATPSGameModeBase::RespawnRequest(AController* Controller)
{
  ResetOnePlayer(Controller);
}

bool ATPSGameModeBase::SetPause(APlayerController* PC, FCanUnpause CanUnpauseDelegate)
{
  bool Pause = Super::SetPause(PC, CanUnpauseDelegate);
  if (Pause)
  {
    StopAllFire();
    SetMatchState(EMatchState::Pause);
  }
  return Pause;
}

bool ATPSGameModeBase::ClearPause()
{
  bool PauseCleared = Super::ClearPause();
  if (PauseCleared)
  {
    SetMatchState(EMatchState::InProgress);
  }
  return PauseCleared;
}

void ATPSGameModeBase::SpawnBots()
{
  if (!GetWorld())
    return;

  for (int32 i = 0; i < GameData.PlayersNum - 1; ++i)
  {
    FActorSpawnParameters SpawnInfo;
    SpawnInfo.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
    const auto BotController = GetWorld()->SpawnActor<AAIController>(AIControllerClass, SpawnInfo);
    RestartPlayer(BotController);
    // UE_LOG(GameModeLog, Display, TEXT("Spawn Controller: %s"), *BotController->GetName());
  }
}

void ATPSGameModeBase::StartRound()
{
  RoundTimer = GameData.RoundsTime;
  GetWorldTimerManager().SetTimer(GameRoundHandle, //
      this, &ATPSGameModeBase::GameTimerUpdate, 1.0f, true);
}

void ATPSGameModeBase::GameTimerUpdate()
{
  //--RoundTimer;
  // UE_LOG(GameModeLog, Display, TEXT("time %i/Round %i/%i"), RoundTimer, CurrentRound, GameData.RoundsNum);

  if (--RoundTimer <= 0)
  {
    GetWorldTimerManager().ClearTimer(GameRoundHandle);
    if (CurrentRound + 1 < GameData.RoundsNum) // ������ ������
    {
      ++CurrentRound;
      ResetPlayers();
      StartRound();
    }
    else // ����� ����
    {
      GameOver();
    }
  }
}

void ATPSGameModeBase::ResetPlayers()
{
  if (!GetWorld())
    return;
  // �������� ���������� � ���� ������������, ������� ��������� � ����,
  // ��������� ���� ���� �������� �� �������, ����������� ��������� ��������� ���������
  for (auto It = GetWorld()->GetControllerIterator(); It; ++It)
  {
    ResetOnePlayer(It->Get());
  }
}

void ATPSGameModeBase::ResetOnePlayer(AController* Controller)
{
  if (Controller && Controller->GetPawn())
  {
    Controller->GetPawn()->Reset();
  }
  RestartPlayer(Controller);
  SetPlayerColor(Controller);
}

void ATPSGameModeBase::CreateTeamsInfo()
{
  if (!GetWorld())
    return;
  int32 TeamID = 1;
  for (auto It = GetWorld()->GetControllerIterator(); It; ++It)
  {
    AController* Controller = It->Get();
    if (!Controller)
      continue;
    ATPSPlayerState* PlayerState = Cast<ATPSPlayerState>(Controller->PlayerState);
    if (!PlayerState)
      continue;
    PlayerState->SetTeamID(TeamID);
    PlayerState->SetTeamColor(DetermineColorByTeamID(TeamID));
    PlayerState->SetPlayerName(Controller->IsPlayerController() ? "Player" : "Bot");
    SetPlayerColor(Controller);
    TeamID = TeamID == 1 ? 2 : 1;
  }
}

FLinearColor ATPSGameModeBase::DetermineColorByTeamID(int32 TeamID) const
{
  if (TeamID - 1 < GameData.TeamColors.Num())
  {
    return GameData.TeamColors[TeamID - 1];
  }
  UE_LOG(GameModeLog, Warning, TEXT("No color ID: %i"), TeamID);
  return GameData.DefaultTeamColor;
}

void ATPSGameModeBase::SetPlayerColor(AController* Controller)
{
  if (!Controller)
    return;
  ATPSCharacter* Character = Cast<ATPSCharacter>(Controller->GetPawn());
  if (!Character)
    return;
  ATPSPlayerState* PlayerState = Cast<ATPSPlayerState>(Controller->PlayerState);
  if (!PlayerState)
    return;
  Character->SetPlayerColor(PlayerState->GetTeamColor());
}

void ATPSGameModeBase::LogPlayerInfo()
{
  if (!GetWorld())
    return;
  for (auto It = GetWorld()->GetControllerIterator(); It; ++It)
  {
    AController* Controller = It->Get();
    if (!Controller)
      continue;
    ATPSPlayerState* PlayerState = Cast<ATPSPlayerState>(Controller->PlayerState);
    if (!PlayerState)
      continue;

    PlayerState->LogInfo();
  }
}

void ATPSGameModeBase::StartRespawn(AController* Controller)
{
  if (RoundTimer <= GameData.RespawnTime)
    return;
  URespawnComponent* RespawnComponent = UTypes::GetPlayerComponent<URespawnComponent>(Controller);
  if (!RespawnComponent || GameData.RespawnTime < 0)
    return;
  RespawnComponent->Respawn(GameData.RespawnTime);
}

void ATPSGameModeBase::GameOver()
{
  UE_LOG(GameModeLog, Display, TEXT("=== Finish Game ==="));
  LogPlayerInfo();
  for (auto Pawn : TActorRange<APawn>(GetWorld()))
  {
    if (Pawn)
    {
      Pawn->TurnOff();
      Pawn->DisableInput(nullptr);
    }
  }
  SetMatchState(EMatchState::GameOver);
}

void ATPSGameModeBase::SetMatchState(EMatchState State)
{
  if (MatchState == State)
    return;

  MatchState = State;
  OnMathStateChanged.Broadcast(MatchState);
}

void ATPSGameModeBase::StopAllFire()
{
    for (auto Pawn : TActorRange<APawn>(GetWorld()))
    {
      const auto WeaponComponent = UTypes::GetPlayerComponent<UWeaponComponent>(Pawn);
    if (!WeaponComponent) 
    {
      continue;
    }
      WeaponComponent->StopFire();
      WeaponComponent->Zoom(false);
    }
}
