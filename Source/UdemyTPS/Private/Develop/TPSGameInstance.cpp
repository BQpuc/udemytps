// https://m.vk.com/id357239644

#include "Develop/TPSGameInstance.h"
#include "Develop/Sound/SoundFuncLib.h"
#include "Sound/SoundMix.h"

// My code, no Udemy code
bool UTPSGameInstance::GetDebuffInfoByName(FName DebuffName, FDebuff& OutInfo) //
{                                                                              //
  bool bIsFind = false;                                                        //
  FDebuff* DebuffInfoRow;                                                      //
  if (DebuffInfoTable)                                                         //
  {                                                                            //
    DebuffInfoRow = DebuffInfoTable->FindRow<FDebuff>(DebuffName, "", false);  //
    if (DebuffInfoRow)                                                         //
    {                                                                          //
      bIsFind = true;                                                          //
      OutInfo = *DebuffInfoRow;                                                //
    }                                                                          //
  }                                                                            //
  else                                                                         //
  {                                                                            //
    UE_LOG(LogTemp, Warning, TEXT("DataTable is not valid in GameInstance"));  //
  }                                                                            //
  return bIsFind;                                                              //
} //

void UTPSGameInstance::ToggleVolume() 
{
  USoundFuncLib::ToggleSoundClassVolume(MasterSoundClass);
}

