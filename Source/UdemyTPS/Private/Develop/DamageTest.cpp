// https://m.vk.com/id357239644

#include "Develop/DamageTest.h"
#include "Kismet/KismetSystemLibrary.h"
#include "Kismet/GameplayStatics.h"
#include "Develop/DamageType/FireDT.h"
#include "Develop/DamageType/IceDT.h"
#include "Develop/DamageType/LightDT.h"
#include "Develop/DamageType/PhysicalDT.h"
#include "Develop/DamageType/PoisonDT.h"
#include "Particles/ParticleSystemComponent.h"

// Sets default values
ADamageTest::ADamageTest()
{
  PrimaryActorTick.bCanEverTick = true;

  SceneComponent = CreateDefaultSubobject<USceneComponent>("SceneComponent");
  SetRootComponent(SceneComponent);

  FX = CreateDefaultSubobject<UParticleSystemComponent>("FX");
  FX->SetupAttachment(SceneComponent);
}

// Called when the game starts or when spawned
void ADamageTest::BeginPlay()
{
  Super::BeginPlay();
  FX->Deactivate();
}

// Called every frame
void ADamageTest::Tick(float DeltaTime)
{
  Super::Tick(DeltaTime);
  DamageTick();
}

void ADamageTest::DamageTick() 
{
  const FVector Location = GetActorLocation();
  UKismetSystemLibrary::DrawDebugSphere(GetWorld(), GetActorLocation(), DamRadius, 12, Color);

  UGameplayStatics::ApplyRadialDamage(GetWorld(), Damage, Location, DamRadius, DamageType, {},
									  this,GetInstigatorController(), FullDamage);
}
