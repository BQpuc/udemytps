


#include "Develop/Menu/MenuGameMode.h"
#include "Develop/Menu/MenuPlayerController.h"
#include "UI/Menu/MenuHUD.h"



AMenuGameMode::AMenuGameMode() 
{
  PlayerControllerClass = AMenuPlayerController::StaticClass();
  HUDClass = AMenuHUD::StaticClass();
}