// https://m.vk.com/id357239644

#include "Player/AI/AI_TPSController.h"
#include "Player/AI/AI_TPSCharacter.h"
#include "Components/TPSAIPerceptionComponent.h"
#include "BehaviorTree/BlackboardComponent.h"
#include "Components/RespawnComponent.h"

AAI_TPSController::AAI_TPSController()
{
  PerceptionComp = CreateDefaultSubobject<UTPSAIPerceptionComponent>("PerseptionComp");
  SetPerceptionComponent(*PerceptionComp);

  RespawnComponent = CreateDefaultSubobject<URespawnComponent>("RespawnComponent");

  bWantsPlayerState = true; // ��������� ��� �� ����� �������� PlayerState, ����� �� ����������� � ��������� ��� ����� ������
}

void AAI_TPSController::Tick(float DeltaTime)
{
  Super::Tick(DeltaTime);

  AActor* AimActor = GetFocusOnActor();
  SetFocus(AimActor);
}

void AAI_TPSController::OnPossess(APawn* InPawn)
{
  Super::OnPossess(InPawn);
  AAI_TPSCharacter* AI_Character = Cast<AAI_TPSCharacter>(InPawn);
  if (!AI_Character)
    return;
  RunBehaviorTree(AI_Character->BehaviorTreeAsset);
}

AActor* AAI_TPSController::GetFocusOnActor() const
{
  if (!GetBlackboardComponent())
    return nullptr;
  return Cast<AActor>(GetBlackboardComponent()->GetValueAsObject(FocusOnKeyName));
}
