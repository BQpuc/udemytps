// https://m.vk.com/id357239644


#include "Player/AI/EQS/EnemyEQCont.h"
#include "EnvironmentQuery/EnvQueryTypes.h"
#include "EnvironmentQuery/Items/EnvQueryItemType_Actor.h"
#include "BehaviorTree/BlackboardComponent.h"
#include "Blueprint/AIBlueprintHelperLibrary.h"


void UEnemyEQCont::ProvideContext(FEnvQueryInstance& QueryInstance, FEnvQueryContextData& ContextData) const
{
  AActor* QueryOwner = Cast<AActor>(QueryInstance.Owner.Get());
  UBlackboardComponent* Blackboard = UAIBlueprintHelperLibrary::GetBlackboard(QueryOwner);
  if (!Blackboard)
    return;
  const UObject* ContextActor = Blackboard->GetValueAsObject(EnemyActorName);
  UEnvQueryItemType_Actor::SetContextHelper(ContextData, Cast<AActor>(ContextActor));
}