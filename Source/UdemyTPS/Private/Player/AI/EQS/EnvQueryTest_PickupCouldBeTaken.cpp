// https://m.vk.com/id357239644


#include "Player/AI/EQS/EnvQueryTest_PickupCouldBeTaken.h"
#include "Components/Pickups/BasePickup.h"
#include "EnvironmentQuery/Items/EnvQueryItemType_ActorBase.h"


UEnvQueryTest_PickupCouldBeTaken::UEnvQueryTest_PickupCouldBeTaken(const FObjectInitializer& ObjectInitializer) //
    : Super(ObjectInitializer)
{
  Cost = EEnvTestCost::Low;
  ValidItemType = UEnvQueryItemType_ActorBase::StaticClass();
  SetWorkOnFloatValues(false);
}

void UEnvQueryTest_PickupCouldBeTaken::RunTest(FEnvQueryInstance& QueryInstance) const 
{
  UObject* DataOwner = QueryInstance.Owner.Get();
  BoolValue.BindData(DataOwner, QueryInstance.QueryID);
  bool WantsBeTakeble = BoolValue.GetValue();


  for (FEnvQueryInstance::ItemIterator It(this, QueryInstance); It; ++It)
  {
    AActor* ItemActor = GetItemActor(QueryInstance, It.GetIndex());
    const auto PickupActor = Cast<ABasePickup>(ItemActor);
    if (!PickupActor)
      continue;
    const auto CouldBeTaken = PickupActor->CouldBeTaken();
    It.SetScore(TestPurpose, FilterType, CouldBeTaken, WantsBeTakeble);
    /*if (CouldBeTaken)
    {
      It.SetScore(TestPurpose, FilterType, true, true);
    }
    else
    {
      It.ForceItemState(EEnvItemStatus::Failed);
    }*/
  }
}