// https://m.vk.com/id357239644


#include "Player/AI/Decorators/HealthPercentDecorator.h"
#include "AIController.h"
#include "Components/HealthComponent.h"


UHealthPercentDecorator::UHealthPercentDecorator()
{
  NodeName = "HealthPercent";
}

bool UHealthPercentDecorator::CalculateRawConditionValue(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory) const
{
  AAIController* Controller = OwnerComp.GetAIOwner();
  if (!Controller)
    return false;

  UHealthComponent* HealthComp = UTypes::GetPlayerComponent<UHealthComponent>(Controller->GetPawn());
  if (!HealthComp || HealthComp->GetIsDead())
    return false;

  return HealthComp->GetHealthPercent() <= HealthPercent;
}
