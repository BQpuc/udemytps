// https://m.vk.com/id357239644


#include "Player/AI/Decorators/NeedAmmoDecorator.h"
#include "Weapon/TPSWeapon.h"
#include "AIController.h"
#include "Components/AIWeaponComponent.h"




UNeedAmmoDecorator::UNeedAmmoDecorator()
{
  NodeName = "NeedAmmo";
}

bool UNeedAmmoDecorator::CalculateRawConditionValue(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory) const
{
  AAIController* Controller = Cast<AAIController>(OwnerComp.GetAIOwner());
  if (!Controller)
  return false;

  UAIWeaponComponent* WeaponComp = UTypes::GetPlayerComponent<UAIWeaponComponent>(Controller->GetPawn());
  if (!WeaponComp)
    return false;

  return WeaponComp->NeedAmmo(WeaponClass);
}
