// https://m.vk.com/id357239644


#include "Player/AI/Services/FireService.h"
#include "AIController.h"
#include "BehaviorTree/BlackboardComponent.h"
#include "Components/WeaponComponent.h"




UFireService::UFireService()
{
  NodeName = "Fire";
}

void UFireService::TickNode(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory, float DeltaSeconds)
{
  AAIController* Controller = OwnerComp.GetAIOwner();
  UBlackboardComponent* Blackboard = OwnerComp.GetBlackboardComponent();
  bool HasAim = Blackboard && Blackboard->GetValueAsObject(EnemyActorKey.SelectedKeyName);
  if (Controller)
  {
    const auto WeaponComp = UTypes::GetPlayerComponent<UWeaponComponent>(Controller->GetPawn());
    if (WeaponComp)
    {
      HasAim ? WeaponComp->StartFire() : WeaponComp->StopFire();
    }
  }
  Super::TickNode(OwnerComp, NodeMemory, DeltaSeconds);
}
