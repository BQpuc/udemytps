// https://m.vk.com/id357239644


#include "Player/AI/Services/ChangeWeaponService.h"
#include "Components/WeaponComponent.h"
#include "AIController.h"


UChangeWeaponService::UChangeWeaponService()
{
  NodeName = "ChangeWeapon";
}

void UChangeWeaponService::TickNode(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory, float DeltaSeconds) 
{
  AAIController* Controller = OwnerComp.GetAIOwner();
  if (Controller)
  {
    UWeaponComponent* Component = UTypes::GetPlayerComponent<UWeaponComponent>(Controller->GetPawn());
    if (Component && FMath::FRand() <= Probability)
    {
      Component->NextWeapon();
    }
  }
  Super::TickNode(OwnerComp, NodeMemory, DeltaSeconds);
}
