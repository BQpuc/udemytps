// https://m.vk.com/id357239644


#include "Player/AI/Services/FindEnemyService.h"
#include "BehaviorTree/BlackboardComponent.h"
#include "AIController.h"
#include "Components/TPSAIPerceptionComponent.h"

UFindEnemyService::UFindEnemyService()
{
  NodeName = "Find Enemy";
}

void UFindEnemyService::TickNode(UBehaviorTreeComponent& OwnerComp,//
    uint8* NodeMemory, float DeltaSeconds)
{
  UBlackboardComponent* Blackboard = OwnerComp.GetBlackboardComponent();
  if (Blackboard)
  {
    AAIController* Controller = OwnerComp.GetAIOwner();
    UTPSAIPerceptionComponent* Perception = UTypes::GetPlayerComponent<UTPSAIPerceptionComponent>(Controller);
    if (Perception)
    {
      Blackboard->SetValueAsObject(EnemyActorKey.SelectedKeyName, Perception->GetClosestEnemy());
    }
  }
  Super::TickNode(OwnerComp, NodeMemory, DeltaSeconds);
}
