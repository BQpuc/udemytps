// https://m.vk.com/id357239644

#include "Player/AI/AI_TPSCharacter.h"
#include "Player/AI/AI_TPSController.h"
#include "Components/TextRenderComponent.h"
#include "Components/AIWeaponComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "BrainComponent.h"
#include "Components/WidgetComponent.h"
#include "UI/HealthBarWidget.h"
#include "Components/HealthComponent.h"

AAI_TPSCharacter::AAI_TPSCharacter(const FObjectInitializer& ObjInit) //
    : Super(ObjInit.SetDefaultSubobjectClass<UAIWeaponComponent>("WeaponComponent"))
{
  AutoPossessAI = EAutoPossessAI::Disabled;
  AIControllerClass = AAI_TPSController::StaticClass();

  bUseControllerRotationYaw = false;
  if (GetCharacterMovement())
  {
    GetCharacterMovement()->bUseControllerDesiredRotation = true;
    GetCharacterMovement()->RotationRate = FRotator(0.0f, 200.0f, 0.0f);
  }
  HealthWidgetComponent = CreateDefaultSubobject<UWidgetComponent>("HealthWidget");
  HealthWidgetComponent->SetupAttachment(GetRootComponent());
  HealthWidgetComponent->SetWidgetSpace(EWidgetSpace::Screen);
  HealthWidgetComponent->SetDrawAtDesiredSize(true);
}

void AAI_TPSCharacter::BeginPlay()
{
  Super::BeginPlay();

  check(HealthWidgetComponent);
  GetWorldTimerManager().SetTimer(HealtnVisHandle, this, //
      &AAI_TPSCharacter::UpdateHealthWidgetVisibility, 1.0f, true);
}

void AAI_TPSCharacter::OnDeath()
{
  Super::OnDeath();
  AAIController* myController = Cast<AAIController>(Controller);
  if (myController && myController->BrainComponent)
  {
    myController->BrainComponent->Cleanup();
  }
  if (HealtnVisHandle.IsValid())
  {
    GetWorldTimerManager().ClearTimer(HealtnVisHandle);
  }
}

void AAI_TPSCharacter::OnHealthChanged(float Health, float Damage)
{
  Super::OnHealthChanged(Health, Damage);

  // �������� ������ ������� �� ����������
  const auto HealthBarWidget = Cast<UHealthBarWidget>(HealthWidgetComponent->GetUserWidgetObject());
  if (!HealthBarWidget)
    return;
  HealthBarWidget->SetHealthPercent(HealthComponent->GetHealthPercent());
}

void AAI_TPSCharacter::UpdateHealthWidgetVisibility()
{
  if (!GetWorld() || !GetWorld()->GetFirstPlayerController(), //
      !GetWorld()->GetFirstPlayerController()->GetPawn())
    return;
  // ���������� Location ������
  const auto PlayerLocation = GetWorld()->GetFirstPlayerController()->GetPawnOrSpectator()->GetActorLocation();
  // ���������� ���������� ����� ����� � �������
  const auto Distance = FVector::Distance(PlayerLocation, GetActorLocation());
  // �������� ������, ���� ����� ������
  HealthWidgetComponent->SetVisibility(Distance < HealthVisibilityDistance);
}
