// https://m.vk.com/id357239644

#include "Player/AI/Task/NextLocationTask.h"
#include "BehaviorTree/BlackboardComponent.h"
#include "AIController.h"
#include "NavigationSystem.h"

UNextLocationTask::UNextLocationTask()
{
  NodeName = "Next Location";
}

EBTNodeResult::Type UNextLocationTask::ExecuteTask(UBehaviorTreeComponent& OwnerComp, //
    uint8* NodeMemory)
{
  const AAIController* Controller = OwnerComp.GetAIOwner();
  UBlackboardComponent* Blackboard = OwnerComp.GetBlackboardComponent();
  if (!Controller || !Blackboard)
    return EBTNodeResult::Failed;

  APawn* Pawn = Controller->GetPawn();
  if (!Pawn)
    return EBTNodeResult::Failed;

  const UNavigationSystemV1* NavSys = UNavigationSystemV1::GetCurrent(Pawn);
  if (!NavSys)
    return EBTNodeResult::Failed;

  FNavLocation NavLocation;
  FVector Location = Pawn->GetActorLocation();
  if (!SelfCenter)
  {
    AActor* CenterActor = Cast<AActor>(Blackboard->GetValueAsObject(CenterActorKey.SelectedKeyName));
    if (!CenterActor)
      return EBTNodeResult::Failed;
    Location = CenterActor->GetActorLocation();
  }
  bool Found = NavSys->GetRandomReachablePointInRadius(Location, Radius, NavLocation);
  if (!Found)
    return EBTNodeResult::Failed;

  Blackboard->SetValueAsVector(AimLocationKey.SelectedKeyName, NavLocation.Location);
  return EBTNodeResult::Succeeded;
}
