// https://m.vk.com/id357239644

#include "Player/PlayerCharacter.h"
#include "Camera/CameraComponent.h"
#include "GameFramework/SpringArmComponent.h"
#include "Components/WeaponComponent.h"
#include "Components/SphereComponent.h"
#include "Components/CapsuleComponent.h"

APlayerCharacter::APlayerCharacter(const FObjectInitializer& ObjInit) : Super(ObjInit)
{
  // Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
  PrimaryActorTick.bCanEverTick = true;

  SpringArm = CreateDefaultSubobject<USpringArmComponent>("CameraSpringArm");
  SpringArm->SetupAttachment(GetRootComponent());
  SpringArm->bUsePawnControlRotation = true;

  CameraComponent = CreateDefaultSubobject<UCameraComponent>("CameraThirdPerson");
  CameraComponent->SetupAttachment(SpringArm);

  CameraCollision = CreateDefaultSubobject<USphereComponent>("CameraCollisionComponent");
  CameraCollision->SetupAttachment(CameraComponent);
  CameraCollision->SetSphereRadius(50.0f);
  CameraCollision->SetCollisionResponseToAllChannels(ECollisionResponse::ECR_Overlap);
}

void APlayerCharacter::BeginPlay()
{
  Super::BeginPlay();

  check(CameraCollision);
  CameraCollision->OnComponentBeginOverlap.AddDynamic(this, &APlayerCharacter::OnComponentBeginOverlap);
  CameraCollision->OnComponentEndOverlap.AddDynamic(this, &APlayerCharacter::OnComponentEndOverlap);
  SpringArm->SocketOffset = FVector(0.0f, 100.0f, 80.0f);
}

// Called to bind functionality to input
void APlayerCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
  Super::SetupPlayerInputComponent(PlayerInputComponent);

  PlayerInputComponent->BindAction("Jump", EInputEvent::IE_Pressed, this, &APlayerCharacter::Jump);
  PlayerInputComponent->BindAction("Sprint", EInputEvent::IE_Pressed, this, &APlayerCharacter::SprintOn);
  PlayerInputComponent->BindAction("Sprint", EInputEvent::IE_Released, this, &APlayerCharacter::SprintOff);
  PlayerInputComponent->BindAction("Fire", EInputEvent::IE_Pressed, WeaponComponent, &UWeaponComponent::StartFire);
  PlayerInputComponent->BindAction("Fire", EInputEvent::IE_Released, WeaponComponent, &UWeaponComponent::StopFire);
  PlayerInputComponent->BindAction("NextWeapon", EInputEvent::IE_Pressed, WeaponComponent, &UWeaponComponent::NextWeapon);
  PlayerInputComponent->BindAction("Reload", EInputEvent::IE_Pressed, WeaponComponent, &UWeaponComponent::Reload);
  DECLARE_DELEGATE_OneParam(FZoomImputSignature, bool);
  PlayerInputComponent->BindAction<FZoomImputSignature>("Zoom", EInputEvent::IE_Pressed, WeaponComponent, &UWeaponComponent::Zoom, true);
  PlayerInputComponent->BindAction<FZoomImputSignature>("Zoom", EInputEvent::IE_Released, WeaponComponent, &UWeaponComponent::Zoom, false);


  PlayerInputComponent->BindAxis("MoveForward", this, &APlayerCharacter::MoveForward);
  PlayerInputComponent->BindAxis("MoveRight", this, &APlayerCharacter::MoveRight);
  PlayerInputComponent->BindAxis("MouseY", this, &APlayerCharacter::AddControllerPitchInput);
  PlayerInputComponent->BindAxis("MouseX", this, &APlayerCharacter::AddControllerYawInput);
}

void APlayerCharacter::OnDeath()
{
  Super::OnDeath();
  if (Controller)
  {
    Controller->ChangeState(NAME_Spectating);
  }
}

void APlayerCharacter::MoveForward(float Scale)
{
  AddMovementInput(GetActorForwardVector(), Scale);
  ScaleForward = Scale;
}

void APlayerCharacter::MoveRight(float Scale)
{
  AddMovementInput(GetActorRightVector(), Scale);
  ScaleRight = Scale;
}

void APlayerCharacter::SprintOn()
{
  bIsSprinting = true;
}

void APlayerCharacter::SprintOff()
{
  bIsSprinting = false;
}

void APlayerCharacter::OnComponentBeginOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp,
    int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
  CheckCameraOverlap();
}

void APlayerCharacter::OnComponentEndOverlap(
    UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{
  CheckCameraOverlap();
}

void APlayerCharacter::CheckCameraOverlap()
{
  bool HideMesh = CameraCollision->IsOverlappingComponent(GetCapsuleComponent());
  GetMesh()->SetOwnerNoSee(HideMesh);
  TArray <USceneComponent*> ChildComponents;
  GetMesh()->GetChildrenComponents(true, ChildComponents);
  for (auto Child : ChildComponents)
  {
    UPrimitiveComponent* ChildMesh = Cast<UPrimitiveComponent>(Child);
    if (ChildMesh)
    {
      ChildMesh->SetOwnerNoSee(HideMesh);
    }
  }
}

bool APlayerCharacter::CanSprint() const
{
  bool Pressed_W = ScaleForward > 0.0f;
  bool NoPressed_A_D = ScaleRight == 0.0f;

  return bIsSprinting && Pressed_W && NoPressed_A_D;
}