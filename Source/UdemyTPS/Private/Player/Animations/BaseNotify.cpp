// https://m.vk.com/id357239644


#include "Player/Animations/BaseNotify.h"

void UBaseNotify::Notify(USkeletalMeshComponent* MeshComp, UAnimSequenceBase* Animation)
{
  OnNotified.Broadcast(MeshComp);
  Super::Notify(MeshComp, Animation);
}