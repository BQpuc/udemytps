// https://m.vk.com/id357239644


#include "Player/TPSPlayerController.h"
#include "Components/RespawnComponent.h"
#include "TPSGameModeBase.h"

ATPSPlayerController::ATPSPlayerController() 
{
  RespawnComponent = CreateDefaultSubobject<URespawnComponent>("RespawnComponent");
}

void ATPSPlayerController::BeginPlay() 
{
  Super::BeginPlay();

  if (GetWorld())
  {
    ATPSGameModeBase* GameMode = Cast<ATPSGameModeBase>(GetWorld()->GetAuthGameMode());
    if (GameMode)
    {
      GameMode->OnMathStateChanged.AddUObject(this, &ATPSPlayerController::OnMatchStateChahged);
    }
  }
}

void ATPSPlayerController::OnPossess(APawn* aPawn) 
{
  Super::OnPossess(aPawn);
  OnNewPawn.Broadcast(aPawn);
}

void ATPSPlayerController::SetupInputComponent()
{
  Super::SetupInputComponent();
  if (!InputComponent)
    return;
  InputComponent->BindAction("Pause", EInputEvent::IE_Pressed, this, &ATPSPlayerController::OnPauseGame);
}

void ATPSPlayerController::OnPauseGame() 
{
  if (!GetWorld() || !GetWorld()->GetAuthGameMode())
    return;
  GetWorld()->GetAuthGameMode()->SetPause(this);
}

void ATPSPlayerController::OnMatchStateChahged(EMatchState State) 
{
  if (State == EMatchState::InProgress) // ���� ��������� � �������� ����
  {
    SetInputMode(FInputModeGameOnly()); // ��������� ���� ��� ������, ����� ������ ����� ���������
    bShowMouseCursor = false; // ������ �� ����������
  }
  else // ���� � ����� ������ ���������
  {
    SetInputMode(FInputModeUIOnly()); // ��������� ��� ������, ����� ������ �����������
    bShowMouseCursor = true; // ���������� ������
  }
}
