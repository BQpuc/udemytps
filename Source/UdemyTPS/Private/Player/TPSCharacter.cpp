// https://m.vk.com/id357239644

#include "Player/TPSCharacter.h"

#include "Components/MyCharacterMovementComponent.h"
#include "Kismet/KismetMathLibrary.h"
#include "Develop/TPSGameInstance.h"
#include "TimerManager.h"
#include "Kismet/GameplayStatics.h"
#include "Particles/ParticleSystemComponent.h"
#include "GameFramework/Controller.h"
#include "Components/WeaponComponent.h"
#include "Components/CapsuleComponent.h"
#include "Sound/SoundCue.h"

DEFINE_LOG_CATEGORY_STATIC(CharacterLog, All, All)

// Sets default values
ATPSCharacter::ATPSCharacter(const FObjectInitializer& ObjInit)
    : Super(ObjInit.SetDefaultSubobjectClass<UMyCharacterMovementComponent>(ACharacter::CharacterMovementComponentName))
{
  // Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
  PrimaryActorTick.bCanEverTick = true;



  HealthComponent = CreateDefaultSubobject<UHealthComponent>("HealthComponent");

  WeaponComponent = CreateDefaultSubobject<UWeaponComponent>("WeaponComponent");



  FireFX = CreateDefaultSubobject<UParticleSystemComponent>("FireFX");
  FireFX->SetupAttachment(GetMesh());
  IceFX = CreateDefaultSubobject<UParticleSystemComponent>("IceFX");
  IceFX->SetupAttachment(GetMesh());
  LightFX = CreateDefaultSubobject<UParticleSystemComponent>("LightFX");
  LightFX->SetupAttachment(GetMesh());
  DarkFX = CreateDefaultSubobject<UParticleSystemComponent>("DarkFX");
  DarkFX->SetupAttachment(GetMesh());
  PoisonFX = CreateDefaultSubobject<UParticleSystemComponent>("PoisonFX");
  PoisonFX->SetupAttachment(GetMesh());
}

// Called when the game starts or when spawned
void ATPSCharacter::BeginPlay()
{
  Super::BeginPlay();

  HealthComponent->DelDamage.AddDynamic(this, &ATPSCharacter::ReactionDamageType);
  HealthComponent->OnDeath.AddUObject(this, &ATPSCharacter::OnDeath);
  HealthComponent->OnChangeHealth.AddUObject(this, &ATPSCharacter::OnHealthChanged);
  HealthComponent->ChangeHealth(0.0f);
  LandedDelegate.AddDynamic(this, &ATPSCharacter::CharFinishFalling);
  FireFX->Deactivate();
  IceFX->Deactivate();
  LightFX->Deactivate();
  DarkFX->Deactivate();
  PoisonFX->Deactivate();

  check(HealthComponent);
  check(GetMesh());


}

// Called every frame
void ATPSCharacter::Tick(float DeltaTime)
{
  Super::Tick(DeltaTime);

  FireDebuffTimer(DeltaTime);
  IceDebuffTimer(DeltaTime);
  LightDebuffTimer(DeltaTime);
  DarkDebuffTimer(DeltaTime);
  PoisonDebuffTimer(DeltaTime);
}





bool ATPSCharacter::CanSprint() const
{
  return false;
}

float ATPSCharacter::GetDirection() const
{
  if (GetVelocity().IsZero())
    return 0.0f;
  const auto Velocity = GetVelocity().GetSafeNormal();
  const auto Dot = FVector::DotProduct(GetActorForwardVector(), Velocity);
  const auto Cross = FVector::CrossProduct(GetActorForwardVector(), Velocity);
  return FMath::RadiansToDegrees(FMath::Acos(Dot)) * FMath::Sign(Cross.Z);
}



void ATPSCharacter::OnDeath()
{
  //UE_LOG(CharacterLog, Display, TEXT("Character DEAD %s"), *GetName());

  GetCharacterMovement()->DisableMovement();
  SetLifeSpan(5.0f);

  GetCapsuleComponent()->SetCollisionResponseToAllChannels(ECollisionResponse::ECR_Ignore);
  WeaponComponent->ToDefaultSMix();
  WeaponComponent->StopFire();
  WeaponComponent->Zoom(false);
  if (GetMesh())
  {
    GetMesh()->SetCollisionEnabled(ECollisionEnabled::QueryAndPhysics);
    GetMesh()->SetSimulatePhysics(true);
  }
  if (DeathSound)
    UGameplayStatics::PlaySoundAtLocation(GetWorld(), DeathSound, GetActorLocation());
}

void ATPSCharacter::OnHealthChanged(float Health, float Damage)
{

}

void ATPSCharacter::TurnOff() 
{
  WeaponComponent->StopFire();
  WeaponComponent->Zoom(false);

  Super::TurnOff();
}

void ATPSCharacter::Reset() 
{
  WeaponComponent->StopFire();
  WeaponComponent->Zoom(false);

  Super::Reset();
}

void ATPSCharacter::SetPlayerColor(const FLinearColor& Color)
{
  UMaterialInstanceDynamic* Material = GetMesh()->CreateAndSetMaterialInstanceDynamic(0);
  if (!Material)
    return;
  Material->SetVectorParameterValue(MaterialColorName, Color);
}

void ATPSCharacter::CharFinishFalling(const FHitResult& Hit)
{
  if (bFallingDamage)
  {
    float SpeedZ = -GetMovementComponent()->Velocity.Z;

    bool IsDamage = SpeedZ > 900.0f;
    UE_LOG(CharacterLog, Display, TEXT("Velocity Z: %.3f Will be Dmg: %d"), SpeedZ, IsDamage);
    if (IsDamage)
    {
      float FallingDamage = (SpeedZ - 900.0f) * 0.1f * FallingForce;
      TakeDamage(FallingDamage, FDamageEvent{}, nullptr, this);
      // HealthComponent->GhangeHealth(FallingDamage);
      UE_LOG(CharacterLog, Display, TEXT("Falling Dmg: %.3f"), FallingDamage);
    }
  }
}



void ATPSCharacter::ReactionDamageType(EDamageType Type, FDebuff Debuff)
{
  switch (Type)
  {
  case EDamageType::Fire:
    FireTimer = Debuff.DebuffMaxTime;
    if (!FireTimerFlag)
    {
      FireTimerFlag = true;
      FireFX->Activate();
    }
    break;
  case EDamageType::Ice:
    IceTimer = Debuff.DebuffMaxTime;
    if (!IceTimerFlag)
    {
      IceTimerFlag = true;
      IceFX->Activate();
    }
    break;
  case EDamageType::Light:
    LightTimer = Debuff.DebuffMaxTime;
    if (!LightTimerFlag)
    {
      LightTimerFlag = true;
      LightFX->Activate();
    }
    break;
  case EDamageType::Dark:
    DarkTimer = Debuff.DebuffMaxTime;
    if (!DarkTimerFlag)
    {
      DarkTimerFlag = true;
      DarkFX->Activate();
    }
    break;
  case EDamageType::Poison:
    PoisonTimer = Debuff.DebuffMaxTime;
    if (!PoisonTimerFlag)
    {
      PoisonTimerFlag = true;
      PoisonFX->Activate();
    }
    break;
  default:
    break;
  }
}

void ATPSCharacter::FireDebuffTimer(float DeltaTime)
{
  if (FireTimerFlag)
  {
    if (FireTimer < 0.0f)
    {
      FireTimerFlag = false;
      FireFX->Deactivate();
    }
    else
    {
      FireTimer -= DeltaTime;
    }
  }
}

void ATPSCharacter::IceDebuffTimer(float DeltaTime)
{
  if (IceTimerFlag)
  {
    if (IceTimer < 0.0f)
    {
      IceTimerFlag = false;
      IceFX->Deactivate();
    }
    else
    {
      IceTimer -= DeltaTime;
    }
  }
}

void ATPSCharacter::LightDebuffTimer(float DeltaTime)
{
  if (LightTimerFlag)
  {
    if (LightTimer < 0.0f)
    {
      LightTimerFlag = false;
      LightFX->Deactivate();
    }
    else
    {
      LightTimer -= DeltaTime;
    }
  }
}

void ATPSCharacter::DarkDebuffTimer(float DeltaTime)
{
  if (DarkTimerFlag)
  {
    if (DarkTimer < 0.0f)
    {
      DarkTimerFlag = false;
      DarkFX->Deactivate();
    }
    else
    {
      DarkTimer -= DeltaTime;
    }
  }
}

void ATPSCharacter::PoisonDebuffTimer(float DeltaTime)
{
  if (PoisonTimerFlag)
  {
    if (PoisonTimer < 0.0f)
    {
      PoisonTimerFlag = false;
      PoisonFX->Deactivate();
    }
    else
    {
      PoisonTimer -= DeltaTime;
    }
  }
}