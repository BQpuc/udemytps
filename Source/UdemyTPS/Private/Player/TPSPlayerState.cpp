// https://m.vk.com/id357239644

#include "Player/TPSPlayerState.h"

DEFINE_LOG_CATEGORY_STATIC(PStateLog, All, All);

void ATPSPlayerState::LogInfo()
{
  UE_LOG(PStateLog, Display, TEXT("TeamID:%i KillsNum:%i DeathsNum:%i"), GetTeamID(), GetKillsNum(), DeathsNum);
}
