// https://m.vk.com/id357239644

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "GameDataWidget.generated.h"

class ATPSPlayerState;
class ATPSGameModeBase;

UCLASS()
class UDEMYTPS_API UGameDataWidget : public UUserWidget
{
  GENERATED_BODY()

  public:


  UFUNCTION(BlueprintCallable, Category = "UI")
  // �������� ����� �������� ������
  int32 GetCurrentRoundNum() const;

  UFUNCTION(BlueprintCallable, Category = "UI")
  // �������� ����� ���-�� �������
  int32 GetTotalRoundsNum() const;

  UFUNCTION(BlueprintCallable, Category = "UI")
  // �������� ���������� ������� ���������� �� ����� ������
  int32 GetRoundSecondsRemaining() const;

  // === PRIVATE ===
  private:
  // �������� ��������� �� GameMode
  ATPSGameModeBase* GetTPSGameMode() const;

  // �������� ��������� �� PlayerState
  ATPSPlayerState* GetTPSPlayerState() const;
};
