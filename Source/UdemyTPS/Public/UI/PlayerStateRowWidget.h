

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "PlayerStateRowWidget.generated.h"

class UImage;
class UTextBlock;

UCLASS()
class UDEMYTPS_API UPlayerStateRowWidget : public UUserWidget
{
  GENERATED_BODY()

  public:
  void SetPlayerName(FText& Text);
  void SetKills(FText& Text);
  void SetDeath(FText& Text);
  void SetTeam(FText& Text);
  void SetPlayerIndicatorVisibility(bool Visible);
  // ��������� ����� �������
  void SetTeamColor(const FLinearColor& Color);

  protected:
  UPROPERTY(meta = (BindWidget))
  UTextBlock* PlayerNameBlock;

  UPROPERTY(meta = (BindWidget))
  UTextBlock* KillsTextBlock;

  UPROPERTY(meta = (BindWidget))
  UTextBlock* DeathTextBlock;

  UPROPERTY(meta = (BindWidget))
  UTextBlock* TeamTextBlock;

  UPROPERTY(meta = (BindWidget))
  UImage* PlayerIndicatorImage;

  UPROPERTY(meta = (BindWidget))
  // ��������, ������������ ���� �������
  UImage* TeamImage;
};
