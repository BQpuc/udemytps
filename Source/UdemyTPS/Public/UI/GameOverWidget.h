

#pragma once

#include "CoreMinimal.h"
#include "UI/BaseWidget.h"
#include "Develop/Types.h"
#include "GameOverWidget.generated.h"

class UVerticalBox;
class UButton;

UCLASS()
class UDEMYTPS_API UGameOverWidget : public UBaseWidget
{
  GENERATED_BODY()

	  public:
  virtual bool Initialize() override;



  protected:
  UPROPERTY(meta = (BindWidget))
  UVerticalBox* PlayerStatBox;

  UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "UI")
  TSubclassOf<UUserWidget> PlayerStatRowWidget;
  UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "UI")
  FName CurrentLevelName = "";

  UPROPERTY(meta = (BindWidget))
  UButton* ResetLevelButton;



  private:
  void OnMatchStateChanhed(EMatchState State);
  void UpdatePlayerStat();

  UFUNCTION()
  void OnResetLevel();
};
