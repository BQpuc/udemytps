// https://m.vk.com/id357239644

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "Develop/Types.h"
#include "SpectatorWidget.generated.h"

/**
 * 
 */
UCLASS()
class UDEMYTPS_API USpectatorWidget : public UUserWidget
{
	GENERATED_BODY()

		public:
  UFUNCTION(BlueprintCallable, Category = "UI")
  // If respawn timer work, return true
  bool GetRespawnTime(int32& CountDownTime) const;

};
