// https://m.vk.com/id357239644

#pragma once

#include "CoreMinimal.h"
#include "UI/BaseWidget.h"
#include "Develop/Types.h"
#include "PauseWidget.generated.h"

class UButton;
class USoundClass;

UCLASS()
class UDEMYTPS_API UPauseWidget : public UBaseWidget
{
  GENERATED_BODY()

  public:
  //
  virtual bool Initialize() override;

  protected:
  UPROPERTY(meta = (BindWidget))
  // ������,  � ���������� ����� ������ ������ ��� "ClearPauseButton"
  UButton* ClearPauseButton;

  UPROPERTY(meta = (BindWidget))
  // ������,  � ���������� ����� ������ ������ ��� "ClearPauseButton"
  UButton* SoundButton;

  UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "SoundSetting")
  FSoundSettingData CharacterSound;
  UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "SoundSetting")
  FSoundSettingData ImpactSound;
  UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "SoundSetting")
  FSoundSettingData MusicSound;
  UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "SoundSetting")
  FSoundSettingData WeaponSound;

  private:
  UFUNCTION()
  // �������� �����. ���������� �� ������� ������
  void OnClearPause();

  UFUNCTION()
  void OnApplaySound();
};
