

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "BaseWidget.generated.h"

class USoundCue;

UCLASS()
class UDEMYTPS_API UBaseWidget : public UUserWidget
{
  GENERATED_BODY()

  public:
  // ��������� ��������
  void Show();

  protected:
  UPROPERTY(meta = (BindWidgetAnim), Transient)
  // �������� �� ����������
  UWidgetAnimation* ShowAnimation;

  UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "UI")
  // ���� ������� ������ ������������� ��� ������� ����
  USoundCue* OpenSound;
};
