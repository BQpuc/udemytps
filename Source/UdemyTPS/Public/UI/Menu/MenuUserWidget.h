

#pragma once

#include "CoreMinimal.h"
#include "UI/BaseWidget.h"
#include "Develop/Types.h"
#include "MenuUserWidget.generated.h"

class UButton;
class UHorizontalBox;
class UTPSGameInstance;
class ULevelItemWidget;
class USoundCue;

UCLASS()
class UDEMYTPS_API UMenuUserWidget : public UBaseWidget
{
  GENERATED_BODY()

  protected:
  virtual void NativeOnInitialized() override;

  // ���������� ��� ���������� ��������
  virtual void OnAnimationFinished_Implementation(const UWidgetAnimation* Animation) override;

   UPROPERTY(meta = (BindWidgetAnim), Transient)
  // �������� ������������ �������� ������
  UWidgetAnimation* HideAnimation;

  UPROPERTY(meta = (BindWidget))
  // ������ �������� �������� ������
  UButton* StartGameButton;

  UPROPERTY(meta = (BindWidget))
  // ������ ������ �� ������� ����
  UButton* QuitGameButton;

  UPROPERTY(meta = (BindWidget))
  // HorizontalBox � ������� ��������� ������� �������
  UHorizontalBox* LevelItemsBox;

  UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "UI")
  // ����� ������������ �������
  TSubclassOf<UUserWidget> LevelItemWidgetClass;

  UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "UI")
  // ���� ������� ������ ������������� ��� ������� �� ������ ������� ����
  USoundCue* StartGameSound;

  private:
  UPROPERTY()
  // ������, � ������� �������� ��������� �� ���������� ������� �������� �������
  TArray<ULevelItemWidget*> LevelItemWidgets;

  UFUNCTION()
  // call back for Start game
  void OnStartGame();

  UFUNCTION()
  // Call back for Quit game
  void OnQuitGame();

  // �������� ������ �������� ������-������ �������
  void InitLevelItems();

  // CallBack ��� �������� �� "LevelItemWidget.h". ���������� ��� ������� �� ������
  void OnLevelSelected(const FLevelData& Data);

  // ���������� ��������� �� ����� GameInstance
  UTPSGameInstance* GetGameInstance() const;
};
