// https://m.vk.com/id357239644

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/HUD.h"
#include "Develop/Types.h"
#include "TPSGameHUD.generated.h"

class UPlayerHUDWidget;
class UBaseWidget;

UCLASS()
class UDEMYTPS_API ATPSGameHUD : public AHUD
{
  GENERATED_BODY()

  public:
  virtual void DrawHUD() override;
  void DrawCross();

  protected:
  virtual void BeginPlay() override;

  UPROPERTY(EditDefaultsOnly, Category = "UI")
  TSubclassOf<UUserWidget> PlayerWidget;

  UPROPERTY(EditDefaultsOnly, Category = "UI")
  TSubclassOf<UUserWidget> PauseWidget;

  UPROPERTY(EditDefaultsOnly, Category = "UI")
  TSubclassOf<UUserWidget> GameOverWidget;

  private:
  UPROPERTY()
  // ������ ��� �������� ���� ��������, ���� EMatchState, �������� UUserWidget*
  TMap<EMatchState, UBaseWidget*> GameWidgets;

  UPROPERTY()
  // ��������� �� ������� ������
  UBaseWidget* CurrentWidget = nullptr;

  void OnMatchStateChahged(EMatchState State);
};
