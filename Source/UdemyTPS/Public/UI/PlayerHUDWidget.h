// https://m.vk.com/id357239644

#pragma once

#include "CoreMinimal.h"
#include "UI/BaseWidget.h"
#include "Develop/Types.h"
#include "PlayerHUDWidget.generated.h"

class UProgressBar;
/**
 *
 */
UCLASS()
class UDEMYTPS_API UPlayerHUDWidget : public UBaseWidget
{
  GENERATED_BODY()

  // ====== PUBLIC ======
  public:
  UFUNCTION(BlueprintCallable, Category = "CallFunctionsCpp") //
  float GetHealthPercent() const;                             // GetHealthPercent()
  UFUNCTION(BlueprintCallable, Category = "CallFunctionsCpp") //
  bool GetWeaponUIData(FWeaponUIData& UIData) const;          // GetWeaponUIData()
  UFUNCTION(BlueprintCallable, Category = "CallFunctionsCpp") //
  FAmmoData GetAmmoData() const;                              // GetAmmoData()
  UFUNCTION(BlueprintCallable, Category = "CallFunctionsCpp") //
  bool IsPlayerAlive() const;                                 // IsPlayerAlive()
  UFUNCTION(BlueprintCallable, Category = "CallFunctionsCpp") //
  bool IsPlayerSpectating() const;                            // IsPlayerSpectating()
  virtual bool Initialize() override;
  UFUNCTION(BlueprintImplementableEvent, Category = "CallFunctionsCpp")
  void OnTakeDamage();
  UFUNCTION(BlueprintCallable, Category = "UI")
  // �������� ���������� ������ ��������� ����������
  int32 GetKillsNum() const;

  UFUNCTION(BlueprintCallable, Category = "UI")
  // ��������� ���� ����� ���������
  FString FormatBullets(int32 Bullets) const;

  protected:
  UPROPERTY(meta = (BindWidget))
  // ������� ��
  UProgressBar* HealthProgressBar;

  UPROPERTY(meta = (BindWidgetAnim), Transient)
  // �������� ��������� ����� (������� ������)
  UWidgetAnimation* DamageAnimation;

  UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "UI", meta = (ClampMin = "0", ClampMax = "1"))
  // ������� ������, ��� ������� �������� ���� �������-����
  float PercentColor = 0.33f;

  UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "UI")
  // ���� ��������-���� (�������)
  FLinearColor GoodColor = FLinearColor::Green;

  UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "UI")
  // ���� ��������-���� ��� ������ ��
  FLinearColor BadColor = FLinearColor::Red;
  private:
  UFUNCTION()
  void OnHealthChanged(float Health, float Damage);
  void OnNewPawn(APawn* NewPawn);
  void UpdateHealthBar();
  // �������� ��������� �� PlayerState
  ATPSPlayerState* GetTPSPlayerState() const;
};
