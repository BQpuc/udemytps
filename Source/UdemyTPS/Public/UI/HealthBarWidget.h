

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "HealthBarWidget.generated.h"

class UProgressBar;


UCLASS()
class UDEMYTPS_API UHealthBarWidget : public UUserWidget
{
  GENERATED_BODY()

  public:
  // �������� ������� ������
  void SetHealthPercent(float Percent);

  protected:
  UPROPERTY(meta = (BindWidget))
  //  ������� �������� ��� �����
  UProgressBar* HealthProgressBar;

  UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "UI", meta = (ClampMin = "0", ClampMax = "1"))
  // ������� ������, ��� ������� ������������ ������� ��������
  float PercentVisibility = 0.99f;

  UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "UI", meta = (ClampMin = "0", ClampMax = "1"))
  // ������� ������, ��� ������� �������� ���� �������-����
  float PercentColor = 0.33f;

  UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "UI")
  // ���� ��������-���� (�������)
  FLinearColor GoodColor = FLinearColor::Green;

  UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "UI")
  // ���� ��������-���� ��� ������ ��
  FLinearColor BadColor = FLinearColor::Red;
};
