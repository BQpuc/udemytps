// https://m.vk.com/id357239644

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "Develop/Types.h"
#include "TPSGameModeBase.generated.h"

DECLARE_MULTICAST_DELEGATE_OneParam(FOnMathStateChangedSignature, EMatchState);

class AAIController;

UCLASS()
class UDEMYTPS_API ATPSGameModeBase : public AGameModeBase
{
  GENERATED_BODY()

  public:
  ATPSGameModeBase();

  FOnMathStateChangedSignature OnMathStateChanged;

  // ������ �� ��, ��� � BeginPlay(), ������ ���� ������
  virtual void StartPlay() override;

  // ������������� ���� �� �������� GameMode. ������������ ������� ������������� ��������� ������ ���������� � ��������� ����������
  virtual UClass* GetDefaultPawnClassForController_Implementation(AController* InController) override;

  // ������� ���������� ������
  void Killed(AController* KillerController, AController* VictimController);

  // �������� �������� ���������.  ��������� FGameData ��  Types
  FGameData GetGameData() const { return GameData; }

  // �������� ����� �������� ������
  int32 GetCurrentRound() const { return CurrentRound; }

  // �������� ���-�� ������, ���������� � ������� ������
  int32 GetRoundSecondsRemaning() const { return RoundTimer; }

  // ������� ���������
  void RespawnRequest(AController* Controller);

  protected:
  // ������������� ���� � ���� �������
  virtual bool SetPause(APlayerController* PC, FCanUnpause CanUnpauseDelegate = FCanUnpause()) override;

  // �������� ���� � �������
  virtual bool ClearPause() override;

  UPROPERTY(EditDefaultsOnly, Category = "Game")
  // ���������� ����
  TSubclassOf<AAIController> AIControllerClass;

  UPROPERTY(EditDefaultsOnly, Category = "Game")
  // �������� ���
  TSubclassOf<APawn> AIPawnClass;

  UPROPERTY(EditDefaultsOnly, Category = "Game")
  // �������� ���������.  ��������� FGameData ��  Types
  FGameData GameData;

  // +++******============ PRIVATE ============******+++
  private:
  // ����� �������� ������
  int32 CurrentRound = 1;

  // ���-�� �������, ���������� � ������� ������
  int32 RoundTimer = 0;

  // ���������� ������� ��� ������� ������� ������
  FTimerHandle GameRoundHandle;

  // ��������� ����. Enum �� Types
  EMatchState MatchState = EMatchState::WaitingToStart;

  // ������� ����, � ������� ������� �����������. ��� ������ ���������� ������������ RestartPlayer()
  void SpawnBots();

  // ��������� ����� �����
  void StartRound();

  // Update �������
  void GameTimerUpdate();

  // �������� ResetOnePlayer() ��� ������� ����������� � �����
  void ResetPlayers();

  // ���������� � ������� ���������
  void ResetOnePlayer(AController* Controller);

  // ������������ ������� �� ��������
  void CreateTeamsInfo();

  // ���������� ���� � ����������� �� �������
  FLinearColor DetermineColorByTeamID(int32 TeamID) const;

  // �������� ��������� �������� ����� ���������
  void SetPlayerColor(AController* Controller);

  // �������� ���������� �� ���� PlayerState
  void LogPlayerInfo();

  // ��������� ������ �������� � RespawnComponent
  void StartRespawn(AController* Controller);

  // ����� ����
  void GameOver();

  // ������������� ��� ��������� ����
  void SetMatchState(EMatchState State);

  // ������������� ��������
  void StopAllFire();
};
