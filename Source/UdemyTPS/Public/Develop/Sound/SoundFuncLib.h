

#pragma once

#include "CoreMinimal.h"
#include "Kismet/BlueprintFunctionLibrary.h"
#include "SoundFuncLib.generated.h"

class USoundClass;

UCLASS()
class UDEMYTPS_API USoundFuncLib : public UBlueprintFunctionLibrary
{
  GENERATED_BODY()

  public:
  UFUNCTION(BlueprintType)
  // ������ ���������
  static void SetSoundClassVolume(USoundClass* SoundClass, float Volume);

  UFUNCTION(BlueprintType)
  // ��������/��������� ����
  static void ToggleSoundClassVolume(USoundClass* SoundClass);
};
