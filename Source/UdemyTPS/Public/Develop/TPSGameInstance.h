// https://m.vk.com/id357239644

#pragma once

#include "CoreMinimal.h"
#include "Engine/GameInstance.h"
#include "Develop/Types.h"
#include "Engine/DataTable.h"
#include "UObject/NameTypes.h"
#include "TPSGameInstance.generated.h"

class USoundClass;
class USoundMix;

UCLASS()
class UDEMYTPS_API UTPSGameInstance : public UGameInstance
{
  GENERATED_BODY()

  public:
  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Debuff") // my code
  UDataTable* DebuffInfoTable = nullptr;                           //
  UFUNCTION(BlueprintCallable)                                     //
  bool GetDebuffInfoByName(FName DebuffName, FDebuff& OutInfo);    //

  // �������� ������ ���������� ������
  FLevelData GetStartupLvl() const { return StartupLevel; }
  // ���������� ������ ���������� ������
  void SetStartupLvl(const FLevelData& Data) { StartupLevel = Data; }

  // �������� ������ ������ �� �������
  TArray<FLevelData> GetLevelsData() const { return LevelsData; }

  // �������� ��� ������ �������� ����
  FName GetMenuLvlName() const { return MenuLevelName; }

  void ToggleVolume();

  UPROPERTY(EditDefaultsOnly, Category = "Sound")
  // ��������� ����� ��� ������
  USoundMix* ExploseSMix;

  protected:
  UPROPERTY(EditDefaultsOnly, Category = "Game")
  // ��� ������ �������� ����
  FName MenuLevelName = NAME_None;

  UPROPERTY(EditDefaultsOnly, Category = "Game", meta = (ToolTip = "Levels names must be unique"))
  // ������ �� ������
  TArray<FLevelData> LevelsData;

  UPROPERTY(EditDefaultsOnly, Category = "Sound")
  // Parrent SoundClass
  USoundClass* MasterSoundClass;

  private:
  // ������ ���������� ������
  FLevelData StartupLevel;
};
