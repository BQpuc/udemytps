// https://m.vk.com/id357239644

#pragma once

#include "Engine/DataTable.h"
#include "Kismet/BlueprintFunctionLibrary.h"
#include "UObject/NameTypes.h"
#include "Animation/AnimMontage.h"
#include "Animation/AnimSequenceBase.h"
#include "Player/TPSPlayerState.h"
#include "Types.generated.h"

DEFINE_LOG_CATEGORY_STATIC(TypesLog, All, All);

class ATPSWeapon;
class UNiagaraSystem;
class USoundCue;

UENUM(BlueprintType)
enum class EMatchState : uint8
{
  WaitingToStart = 0,
  InProgress,
  Pause,
  GameOver
};



UENUM(BlueprintType)
enum class EDamageType : uint8
{
  Fire UMETA(DisplayName = "Fire"),
  Ice UMETA(DisplayName = "Ice"),
  Light UMETA(DisplayName = "Light"),
  Dark UMETA(DisplayName = "Dark"),
  Poison UMETA(DisplayName = "Poison")
};

USTRUCT(BlueprintType)
struct FDebuff : public FTableRowBase
{
  GENERATED_BODY()

  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Damage")
  // ��� ������ (Enum)
  EDamageType Type = EDamageType::Fire;
  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Damage", meta = (ClampMin = "0.5", ClampMax = "180.0"))
  // ������������ ����� ������
  float DebuffMaxTime = 5.0f;
  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Damage", meta = (ClampMin = "0.01667", ClampMax = "15.0"))
  // �������� ��� ������� ������
  float TickInterval = 0.5f;
  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Damage", meta = (ClampMin = "0.0", ClampMax = "1000000000.0"))
  // ����� ��� ������ �������
  float DamagePerTick = 1.0f;
};

USTRUCT(BlueprintType)
struct FAmmoData
{
  GENERATED_USTRUCT_BODY()

  UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Weapon", meta = (ClampMin = "1", ClampMax = "1000"))
  // ���������� �������� � ��������
  int32 Bullets = 30;
  UPROPERTY(
      EditDefaultsOnly, BlueprintReadWrite, Category = "Weapon", meta = (ClampMin = "1", ClampMax = "1000", EditCondition = "!Infinite"))
  // ���������� ���������
  int32 Clips = 10;
  UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Weapon")
  // ���������� �� �������
  bool Infinite = false;
};

USTRUCT(BlueprintType)
struct FWeaponData
{
  GENERATED_USTRUCT_BODY()

  UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Weapon")
  // �������� ������
  TSubclassOf<ATPSWeapon> WeaponClass;
  UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Weapon")
  // �������� �����������
  UAnimMontage* ReloadAnimMontage;
};

USTRUCT(BlueprintType)
struct FWeaponUIData
{
  GENERATED_USTRUCT_BODY()

  UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "UI")
  // �������� �����/��������
  UTexture2D* MainIcon;
  UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "UI")
  // ������ �������
  UTexture2D* CrossHairIcon;
};

USTRUCT(BlueprintType)
struct FRotation
{
  GENERATED_USTRUCT_BODY()

  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Move")
  // ������� �� �������
  bool IsRotating = true;
  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Move", meta = (ClampMin = "0.0", ClampMax = "10.0", EditCondition = "IsRotating"))
  // ����������� �������� ��������
  float MinAngle = 0.5f;
  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Move", meta = (ClampMin = "0.0", ClampMax = "10.0", EditCondition = "IsRotating"))
  // ����������� �������� ��������
  float MaxAngle = 3.0f;

  UPROPERTY(EditAnywhere, Category = "Move")
  // ��������� �����-���� ��� ���
  bool IsSinMove = true;
  UPROPERTY(EditAnywhere, Category = "Move", meta = (ClampMin = "0.0", EditCondition = "IsSinMove"))
  // ������, �� ������� ����� �������/��������
  float Amplitude = 30.0f;
  UPROPERTY(EditAnywhere, Category = "Move", meta = (ClampMin = "0.0", ClampMax = "30.0", EditCondition = "IsSinMove"))
  // ���������� ��������� � �������
  float Frequency = 2.0f;
};

USTRUCT(BlueprintType)
struct FDecalData
{
  GENERATED_USTRUCT_BODY()

  UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "VFX")
  // ������, �� ��������
  UMaterialInterface* Material;
  UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "VFX")
  // ������ ������
  FVector Size = FVector(10.0f);
  UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "VFX")
  // ����� ����� ������
  float LifeTime = 5.0f;
  UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "VFX")
  // �����, �� ������� ������ ���������� ��������
  float FadeOutTime = 0.7f;
};

USTRUCT(BlueprintType)
struct FImpactData
{
  GENERATED_USTRUCT_BODY()

  UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "VFX")
  // ������� FX ��� ��������� ���� ��� ������
  UNiagaraSystem* NiagaraEffect;
  UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "VFX")
  // ��������� FDecalData �� Types, ��� ��������� ���� ��� ������
  FDecalData DecalData;
  UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "VFX")
  // ���� ��������� ����
  USoundCue* Sound;
};

// General settings GameMode
USTRUCT(BlueprintType)
struct FGameData
{
  GENERATED_USTRUCT_BODY()

  UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Game", meta = (ClampMin = "1", ClampMax = "100"))
  // ���������� ����� � ������� � ���� ��������
  int32 PlayersNum = 2;
  
  UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Game", meta = (ClampMin = "1", ClampMax = "10"))
  // ������������ ���������� �������
  int32 RoundsNum = 2;
  
  UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Game", meta = (ClampMin = "3", ClampMax = "1800"))
  // ������������ ����� �����
  int32 RoundsTime = 3;
  
  UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
  // ��������� ���� �������
  FLinearColor DefaultTeamColor = FLinearColor::Gray;
  
  UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
  // ����� ��� ������
  TArray<FLinearColor> TeamColors;
  
  UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Game", meta = (ClampMin = "-1", ClampMax = "60"))
  // ����� �� �����������
  int32 RespawnTime = -1;
};

// ������ ��� LevelItemWidget
USTRUCT(BlueprintType)
struct FLevelData
{
  GENERATED_USTRUCT_BODY()

   UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Game")
   FName LevelName = NAME_None; // �������� ������

   UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Game")
   FName LevelDisplayName = NAME_None;  // ������������ �� �������� ��� ������

   UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Game")
   UTexture2D* LevelThumb;   // �������� ������������ �� ��������
};

USTRUCT(BlueprintType)
struct FSoundSettingData
{
  GENERATED_USTRUCT_BODY()

  UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "SoundSetting")
  float Volume = 0.75f;

  UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "SoundSetting")
  USoundClass* SoundClass;

};

DECLARE_MULTICAST_DELEGATE_OneParam(FOnLevelSelectedSignature, const FLevelData&);

UCLASS()
class UDEMYTPS_API UTypes : public UBlueprintFunctionLibrary
{
  GENERATED_BODY()

  public:
  bool static AreEnemies(AController* Cont1, AController* Cont2)
  {
    if (!Cont1 || !Cont2 || Cont1 == Cont2)
    {
      //UE_LOG(TypesLog, Warning, TEXT("%s %s"), *Cont1->GetName(),  *Cont2->GetName());
      return false;
    }

    const auto State1 = Cast<ATPSPlayerState>(Cont1->PlayerState);
    const auto State2 = Cast<ATPSPlayerState>(Cont2->PlayerState);
   // UE_LOG(TypesLog, Display, TEXT("%s, %s"), *State1->GetName(), *State2->GetName());
    return State1 && State2 && (State1->GetTeamID() != State2->GetTeamID());
  }

  template <typename T>                                     //
  static T* FindNotifyByClass(UAnimSequenceBase* Animation) //
  {                                                         //
    if (!Animation)                                         //
    {                                                       //
      return nullptr;                                       //
    }                                                       //
    const auto NotifyEvents = Animation->Notifies;          //
    for (auto NotifyEvent : NotifyEvents)                   //
    {                                                       //
      auto AnimNotify = Cast<T>(NotifyEvent.Notify);        //
      if (AnimNotify)                                       //
      {                                                     //
        return AnimNotify;                                  //
      }                                                     //
    }                                                       //
    return nullptr;                                         //
  }                                                         //

  template <typename T> static T* GetPlayerComponent(AActor* Pawn)
  {
    if (!Pawn)
      return nullptr;
    const auto Component = Pawn->GetComponentByClass(T::StaticClass());
    return Cast<T>(Component);
  }

  static FText TextFromInt(int32 Number) 
  { 
      return FText::FromString(FString::FromInt(Number)); 
  }
};
