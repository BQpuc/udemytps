// https://m.vk.com/id357239644

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "DamageTest.generated.h"

UCLASS()
class UDEMYTPS_API ADamageTest : public AActor
{
  GENERATED_BODY()

  public:
  // Sets default values for this actor's properties
  ADamageTest();

  UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Components")
  USceneComponent* SceneComponent;

  UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Components")
  UParticleSystemComponent* FX;

  protected:
  // Called when the game starts or when spawned
  virtual void BeginPlay() override;

  public:
  // Called every frame
  virtual void Tick(float DeltaTime) override;

  UFUNCTION()
  void DamageTick();

  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Damage", meta = (ClampMin = "0.0", ClampMax = "1000.0"))
  float DamRadius = 100.0f;

  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Damage")
  FLinearColor Color = FLinearColor::Red;

  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Damage", meta = (ClampMin = "0.0", ClampMax = "1000.0"))
  float Damage = 0.5;

  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Damage")
  bool FullDamage = false;

  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Damage")
  TSubclassOf<UDamageType> DamageType;
};
