// Shoot Them Up Game, All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Develop/Types.h"
#include "TPSWeapon.generated.h"

DECLARE_MULTICAST_DELEGATE_OneParam(FOnClipEmptySignature, ATPSWeapon*);

class USkeletalMeshComponent;
class UNiagaraSystem;
class UNiagaraComponent;
class USoundCue;

UCLASS()
class UDEMYTPS_API ATPSWeapon : public AActor
{
  GENERATED_BODY()

  // ====== PUBLIC ======
  public:
  ATPSWeapon();

  FOnClipEmptySignature OnClipEmpty;

  virtual void StartFire();
  virtual void StopFire();

  void ChangeClip();                                    // ChangeClip()
  bool CanReload() const;                               // CanReload()
  FAmmoData GetAmmoData() const { return CurrentAmmo; } // GetAmmoData()
  UFUNCTION(BlueprintCallable)                          //
  FWeaponUIData GetUIData() const { return UIData; }    // GetUIData()
  bool TryToAddAmmo(int32 ClipsAmount);                 // TryToAddAmmo()
  bool IsAmmoEmpty() const;                             // Return true when no ammo

  // ������ ���� ������ ������
 virtual void Zoom(bool Enabled);

  // ====== PROTECTED ======
  protected:
  virtual void BeginPlay() override;

  UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Components") //
  USkeletalMeshComponent* WeaponMesh;                                     // WeaponMesh
  UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Weapon")    //
  FName MuzzleSocketName = "MuzzleSocket";                                // MuzzleSocketName
  UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Weapon")    //
  float TraceMaxDistance = 60000.0f;                                      // TraceMaxDistance
  UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Weapon")    //
  FAmmoData DefaultAmmo;                                                  // DefaultAmmo
  UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "UI")        //
  FWeaponUIData UIData;                                                   // UIData
  UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "VFX")       //
  UNiagaraSystem* MuzzleFX;                                               // MuzzleFX

  UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Sound")
  // ���� ��������� �� ����
  USoundCue* FireSound;

  UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Sound")
  // ���� ��������������� ��� ���������� ����������
  USoundCue* NoAmmoSound;

 

  virtual void MakeShot();                                                 // MakeShot()
  virtual bool GetTraceData(FVector& TraceStart, FVector& TraceEnd) const; // GetTraceData()
  // APlayerController* GetPlayerController() const;                               // GetPlayerController()
  bool GetPlayerViewPoint(FVector& ViewLocation, FRotator& ViewRotation) const; // GetPlayerViewPoint()
  FVector GetMuzzleWorldLocation() const;                                       // GetMuzzleWorldLocation()

  void MakeHit(FHitResult& HitResult, const FVector& TraceStart, const FVector& TraceEnd);
  void DecreaseAmmo(); // minus bullets

  bool IsClipEmpty() const;           // Return true when magazine does not have ammo
  void LogAmmo();                     // Print debug message
  bool IsAmmoFull() const;            // return true when max Ammo and Bullets
  UNiagaraComponent* SpawnMuzzleFX(); // Spawn Niagara effect

  // ====== PRIVATE ======
  private:
  FAmmoData CurrentAmmo;
};
