// Shoot Them Up Game, All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Projectile.generated.h"

class USphereComponent;
class UProjectileMovementComponent;
class UWeaponFXComponent;

UCLASS()
class UDEMYTPS_API AProjectile : public AActor 
{
  GENERATED_BODY()

  public:
  AProjectile(); // ASTUProjectile();

  void SetShotDirection(const FVector& Direction) { ShotDirection = Direction; }

  protected:
  UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = "Components")
  USphereComponent* CollisionComponent;

  UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = "Components")
  UProjectileMovementComponent* MovementComponent = nullptr;

  UPROPERTY(VisibleAnywhere, Category = "Components")
  UWeaponFXComponent* WeaponFXComponent;

  virtual void BeginPlay() override;

  UPROPERTY(BlueprintReadWrite)
  FVector ShotDirection;

   UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Weapon")
  float LifeTime = 5.0f;

   AController* GetController() const;
  
  public:
  float DamageRadius = 0.0f;
  float DamageAmount = 0.0f;
  bool IsFullDamage = true;
  UPROPERTY()
  TSubclassOf<UDamageType> DamageType = UDamageType::StaticClass();
  UFUNCTION()
  void OnProjectileHit(UPrimitiveComponent* HitComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit);

};

/* https://m.vk.com/id357239644


class USphereComponent;
class UProjectileMovementComponent;

UCLASS()
class UDEMYTPS_API AProjectile : public AActor
{
  GENERATED_BODY()

  public:
  AProjectile();


  protected:
  virtual void BeginPlay() override;

      UPROPERTY(VisibleDefaultsOnly, Category = "Weapon")
  UProjectileMovementComponent* MovementComponent;

  UPROPERTY(VisibleDefaultsOnly, Category = "Components")
  USphereComponent* SphereCollission;

  public:




  void SetShotDirection(const FVector& Diraction);

  UPROPERTY()
    FVector ShotDirection;// = FVector(0.0f, 0.0f, 0.0f);
};
*/