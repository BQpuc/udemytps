

// Shoot Them Up Game, All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "Weapon/TPSWeapon.h"
#include "RifleWeapon.generated.h"

class UWeaponFXComponent;
class UNiagaraComponent;
class UNiagaraSystem;
class USoundCue;

UCLASS()
class UDEMYTPS_API ARifleWeapon : public ATPSWeapon // class SHOOTTHEMUP_API ASTURifleWeapon : public ASTUBaseWeapon
{
  GENERATED_BODY()

  public:
  ARifleWeapon();

  virtual void StartFire() override;
  virtual void StopFire() override;
  // ���������� ���������� ��������� GetOwner()
  AController* GetController() const;

  // �����������/��������� ������
  virtual void Zoom(bool Enabled) override;

  UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Weapon", meta = (ClampMin = "1", ClampMax = "10"))
  // �� ������� ��� ����� ���������� ������
  float ZoomModifier = 2.0f;

  protected:
  virtual void BeginPlay() override;
  virtual bool GetTraceData(FVector& TraceStart, FVector& TraceEnd) const override;

  UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Weapon") //
  float TimeBetweenShots = 0.1f;                                       // TimeBetweenShots
  UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Weapon") //
  float BulletSpread = 1.5f;                                           // BulletSpread
  UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Weapon") //
  float DamageAmount = 10.0f;                                          // DamageAmount
  UPROPERTY(VisibleAnywhere, Category = "Components")                  //
  UWeaponFXComponent* WeaponFXComponent;                               // WeaponFXComponent
  UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "VFX")    //
  UNiagaraSystem* TraceFX;                                             // TraceFX
  UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "VFX")    //
  FString TraceTargetName = "TraceTarget";                             // TraceTargetName
  virtual void MakeShot() override;                                    // MakeShot()
  void MakeDamage(const FHitResult& HitResult);                        // MakeDamage()

  private:
  FTimerHandle ShotTimerHandle;

  UPROPERTY()
  // ���������� ������ ���������
  UNiagaraComponent* MuzzleFXComponent;

  // ���� ������ ��� ������ ����
  float StartFOV = 90.0f;

  // ������� ������ ��������� � ���� ���������
  void InitFX();
  // ������������� ��� ��������� ���� � ������ ���������
  void SetFxActive(bool Visible);
  void SpawnTraceFX(const FVector& TraceStart, const FVector& TraceEnd);
};
