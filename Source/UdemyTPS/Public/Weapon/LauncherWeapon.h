// Shoot Them Up Game, All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "Weapon/TPSWeapon.h"
#include "LauncherWeapon.generated.h"

class AProjectile;

UCLASS()
class UDEMYTPS_API ALauncherWeapon : public ATPSWeapon
{
  GENERATED_BODY()

  public:
  virtual void StartFire() override;

  protected:
  UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Weapon")
  TSubclassOf<AProjectile> ProjectileClass;

  virtual void MakeShot() override;

  public:
  UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Weapon")
  float ExploseRadius = 300.0f;
  UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Weapon")
  float MaxDamage = 100.0f;
  UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Weapon")
  TSubclassOf<UDamageType> DamageType = UDamageType::StaticClass();
  UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Weapon")
  bool IsFullDamage = true;
};

/*

class AProjectile;

UCLASS()
class UDEMYTPS_API ALauncherWeapon : public ATPSWeapon
{
  GENERATED_BODY()

  public:
  virtual void StartFire() override;

  protected:
  UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Projectile")
  TSubclassOf<AProjectile> ProjectileClass;
  virtual void MakeShot() override;
};*/
