// https://m.vk.com/id357239644

#pragma once

#include "CoreMinimal.h"
#include "Animation/AnimNotifies/AnimNotify.h"
#include "BaseNotify.generated.h"

DECLARE_MULTICAST_DELEGATE_OneParam(FOnNotifySignature, USkeletalMeshComponent*);


UCLASS()
class UDEMYTPS_API UBaseNotify : public UAnimNotify
{
	GENERATED_BODY()
	
		public:
  virtual void Notify(USkeletalMeshComponent* MeshComp, UAnimSequenceBase* Animation) override;
  FOnNotifySignature OnNotified;
};
