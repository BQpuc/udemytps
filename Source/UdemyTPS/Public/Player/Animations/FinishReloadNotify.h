// https://m.vk.com/id357239644

#pragma once

#include "CoreMinimal.h"
#include "Player/Animations/BaseNotify.h"
#include "FinishReloadNotify.generated.h"

/**
 * 
 */
UCLASS()
class UDEMYTPS_API UFinishReloadNotify : public UBaseNotify
{
	GENERATED_BODY()
	
};
