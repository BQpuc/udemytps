

#pragma once

#include "CoreMinimal.h"
#include "Player/Animations/BaseNotify.h"
#include "MyFootstepNotify.generated.h"

/**
 * 
 */
UCLASS()
class UDEMYTPS_API UMyFootstepNotify : public UBaseNotify
{
	GENERATED_BODY()
	
};
