// https://m.vk.com/id357239644

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "Components/HealthComponent.h"
#include "Develop/Types.h"
#include "TPSCharacter.generated.h"

class UHealthComponent;
class UWeaponComponent;
class USoundCue;

UCLASS()
class UDEMYTPS_API ATPSCharacter : public ACharacter
{
  GENERATED_BODY()

  public:
  ATPSCharacter(const FObjectInitializer& ObjInit);

  UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Components")
  UHealthComponent* HealthComponent;

  UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Components")
  UWeaponComponent* WeaponComponent;

  UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Components")
  UParticleSystemComponent* FireFX;
  UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Components")
  UParticleSystemComponent* IceFX;
  UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Components")
  UParticleSystemComponent* LightFX;
  UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Components")
  UParticleSystemComponent* DarkFX;
  UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Components")
  UParticleSystemComponent* PoisonFX;

  // ====== PARRENT FUNCTIONS ======
  protected:
  virtual void BeginPlay() override;

  UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Sound")
  USoundCue* DeathSound;

  public:
  virtual void Tick(float DeltaTime) override;

  // ���������� ��� ��������� ��������
  virtual void OnHealthChanged(float Health, float Damage);

  // ====== PUBLIC ======
  public:
  virtual void TurnOff() override;
  virtual void Reset() override;


  UFUNCTION(BlueprintCallable)
  virtual bool CanSprint() const;

  UFUNCTION(BlueprintCallable)
  float GetDirection() const;

  UFUNCTION()
  void ReactionDamageType(EDamageType DamType, FDebuff Debuff);

  UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Falling")
  bool bFallingDamage = true;
  UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Falling",
      meta = (ClampMin = "0.0", ClampMax = "100.0", EditCondition = "bFallingDamage"))
  float FallingForce = 1.0f;
  // ������������� �������� ���������
  void SetPlayerColor(const FLinearColor& Color);

  // ===== PRIVATE =====
  private:
  UFUNCTION()
  void CharFinishFalling(const FHitResult& Hit);

  //  ===== PROTECTED =====
  protected:
  virtual void OnDeath();
  UPROPERTY(EditDefaultsOnly, Category = "Material")
  // ��� ��������� ��������� ��� ����� �����
  FName MaterialColorName = "Paint Color";

  // DEBUFF
  float FireTimer;
  float IceTimer;
  float LightTimer;
  float DarkTimer;
  float PoisonTimer;
  bool FireTimerFlag = false;
  bool IceTimerFlag = false;
  bool LightTimerFlag = false;
  bool DarkTimerFlag = false;
  bool PoisonTimerFlag = false;
  void FireDebuffTimer(float DeltaTime);
  void IceDebuffTimer(float DeltaTime);
  void LightDebuffTimer(float DeltaTime);
  void DarkDebuffTimer(float DeltaTime);
  void PoisonDebuffTimer(float DeltaTime);
};
