// https://m.vk.com/id357239644

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "Develop/Types.h"
#include "TPSPlayerController.generated.h"

class URespawnComponent;

UCLASS()
class UDEMYTPS_API ATPSPlayerController : public APlayerController
{
  GENERATED_BODY()

  public:
  ATPSPlayerController();

  protected:
  virtual void BeginPlay() override;

  UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Components")
  URespawnComponent* RespawnComponent;

  virtual void OnPossess(APawn* aPawn) override;

  // ����� ���������� �������� ������ � �������
  virtual void SetupInputComponent() override;

  private:
  // ����� ����� ��� ������� ������ "P"
  void OnPauseGame();

  // ���������� ��� ��������� ��������� ����. ����������/�������� ������
  void OnMatchStateChahged(EMatchState State);
};
