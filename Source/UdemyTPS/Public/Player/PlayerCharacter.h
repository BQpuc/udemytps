// https://m.vk.com/id357239644

#pragma once

#include "CoreMinimal.h"
#include "Player/TPSCharacter.h"
#include "PlayerCharacter.generated.h"

class UCameraComponent;
class USpringArmComponent;
class USphereComponent;

UCLASS()
class UDEMYTPS_API APlayerCharacter : public ATPSCharacter
{
	GENERATED_BODY()

  // ========= ========= PUBLIC ========= =========
		public:
  APlayerCharacter(const FObjectInitializer& ObjInit);

  virtual bool CanSprint() const override;


  // ========= ========= PROTECTED ========= =========
  protected:
  virtual void BeginPlay() override;

  virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

  UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Components")
  USpringArmComponent* SpringArm;

  UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Components")
  UCameraComponent* CameraComponent;

  UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Components")
  USphereComponent* CameraCollision;

  UPROPERTY(BlueprintReadOnly, Category = "Movement")
  bool bIsSprinting = false;

   virtual void OnDeath() override;

  // ========= ========= PRIVATE ========= =========
   private:
  float ScaleRight = 0.0f;
  float ScaleForward = 0.0f;
  void MoveForward(float Scale);
  void MoveRight(float Scale);
  void SprintOn();
  void SprintOff();

  UFUNCTION()
   void OnComponentBeginOverlap (UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);
  UFUNCTION()
   void OnComponentEndOverlap (UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex);
   void CheckCameraOverlap();
};
