// https://m.vk.com/id357239644

#pragma once

#include "CoreMinimal.h"
#include "Player/TPSCharacter.h"
#include "AI_TPSCharacter.generated.h"

class UBehaviorTree;
class UWidgetComponent;

UCLASS()
class UDEMYTPS_API AAI_TPSCharacter : public ATPSCharacter
{
  GENERATED_BODY()

  public:
  AAI_TPSCharacter(const FObjectInitializer& ObjInit);

  // ====== PUBLIC ======
  UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "AI")
  UBehaviorTree* BehaviorTreeAsset;

  // ====== PROTECTED ======
  protected:
  virtual void BeginPlay() override;
  virtual void OnDeath() override;
  // ���������� ��� ��������� ��������
  virtual void OnHealthChanged(float Health, float Damage) override;

  UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Components")
  // ������ �������� �� ������� ����
  UWidgetComponent* HealthWidgetComponent;

  UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "AI")
  // ���������� ��� ������� ������ ���������� ���������
  float HealthVisibilityDistance = 1300.0f;

  private:
	  // ������ ������ �� ���������, ���� ����� ������
  void UpdateHealthWidgetVisibility();
      FTimerHandle HealtnVisHandle;
};
