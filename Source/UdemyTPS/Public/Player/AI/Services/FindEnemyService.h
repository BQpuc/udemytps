// https://m.vk.com/id357239644

#pragma once

#include "CoreMinimal.h"
#include "BehaviorTree/BTService.h"
#include "Develop/Types.h"
#include "FindEnemyService.generated.h"

/**
 *
 */
UCLASS()
class UDEMYTPS_API UFindEnemyService : public UBTService
{
  GENERATED_BODY()

  public:
  UFindEnemyService();

  protected:
  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "AI")
  FBlackboardKeySelector EnemyActorKey;

  virtual void TickNode(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory, float DeltaSeconds) override;
};
