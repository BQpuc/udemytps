// https://m.vk.com/id357239644

#pragma once

#include "CoreMinimal.h"
#include "BehaviorTree/BTService.h"
#include "Develop/Types.h"
#include "ChangeWeaponService.generated.h"

/**
 * 
 */
UCLASS()
class UDEMYTPS_API UChangeWeaponService : public UBTService
{
	GENERATED_BODY()
	
public:
  UChangeWeaponService();

protected:
  virtual void TickNode(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory, float DeltaSeconds) override;

  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "AI", meta = (ClampMin = "0.0", ClampMax = "1.0"))
  float Probability = 0.1f;
};
