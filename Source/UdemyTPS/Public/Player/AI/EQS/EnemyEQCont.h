// https://m.vk.com/id357239644

#pragma once

#include "CoreMinimal.h"
#include "EnvironmentQuery/EnvQueryContext.h"
#include "EnemyEQCont.generated.h"

UCLASS()
class UDEMYTPS_API UEnemyEQCont : public UEnvQueryContext
{
  GENERATED_BODY()

  public:
  virtual void ProvideContext(FEnvQueryInstance& QueryInstance, FEnvQueryContextData& ContextData) const override;

  protected:
  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "AI")
  FName EnemyActorName = "EnemyActor";


};
