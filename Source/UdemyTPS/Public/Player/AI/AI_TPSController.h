// https://m.vk.com/id357239644

#pragma once

#include "CoreMinimal.h"
#include "AIController.h"
#include "AI_TPSController.generated.h"

class UTPSAIPerceptionComponent;
class URespawnComponent;

UCLASS()
class UDEMYTPS_API AAI_TPSController : public AAIController
{
  GENERATED_BODY()

  public:
  AAI_TPSController();
      virtual void Tick(float DeltaTime) override;

  protected:
  virtual void OnPossess(APawn* InPawn) override;

  UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Components")
  UTPSAIPerceptionComponent* PerceptionComp;

  UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Components")
  URespawnComponent* RespawnComponent;

  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "AI")
  FName FocusOnKeyName = "EnemyActor";

  private:
  AActor* GetFocusOnActor() const;
};
