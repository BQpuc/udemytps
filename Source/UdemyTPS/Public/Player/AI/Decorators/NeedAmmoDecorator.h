// https://m.vk.com/id357239644

#pragma once

#include "CoreMinimal.h"
#include "BehaviorTree/BTDecorator.h"
#include "Develop/Types.h"
#include "NeedAmmoDecorator.generated.h"

class ATPSWeapon;

UCLASS()
class UDEMYTPS_API UNeedAmmoDecorator : public UBTDecorator
{
  GENERATED_BODY()

  public:
  UNeedAmmoDecorator();

  protected:
  virtual bool CalculateRawConditionValue(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory) const override;

  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "AI")
  TSubclassOf<ATPSWeapon> WeaponClass;
};
