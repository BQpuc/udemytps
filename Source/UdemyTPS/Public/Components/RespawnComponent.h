// https://m.vk.com/id357239644

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "RespawnComponent.generated.h"


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class UDEMYTPS_API URespawnComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	URespawnComponent();

protected:
	virtual void BeginPlay() override;

public:	
	// ������ ������� ��������
    void Respawn(int32 RespawnTime);

	  // �������� ���������� ������ �� ��������
    int32 GetRespawnCountDown() const { return RespawnCountDown; }

	// ����� ������ �������� ������� ���������� true
    bool IsRespawnInProgress() const;
		
	private:
		// ���������� ������� ��������
      FTimerHandle RespawnHandle;

	  // ���������� ����� �� ��������
      int32 RespawnCountDown = 0;

	  // ���������� �������
      void RespawnTimerUpdate();


};
