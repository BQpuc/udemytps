// https://m.vk.com/id357239644

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "Develop/Types.h"
#include "WeaponComponent.generated.h"

class ATPSWeapon;
class UTPSGameInstance;

UCLASS(ClassGroup = (Custom), meta = (BlueprintSpawnableComponent))
class UDEMYTPS_API UWeaponComponent : public UActorComponent
{
  GENERATED_BODY()

  public:
  UWeaponComponent();

  protected:
  virtual void BeginPlay() override;

  UPROPERTY(EditDefaultsOnly, Category = "Weapon")
  FName WeaponEquipSocketName = "WeaponPoint";

  UPROPERTY(EditDefaultsOnly, Category = "Weapon")
  TArray<FWeaponData> WeaponData;

  UPROPERTY(EditDefaultsOnly, Category = "Weapon")
  FName WeaponArmorySocketName = "ArmorySocket";

  UPROPERTY(EditDefaultsOnly, Category = "Weapon")
  UAnimMontage* EquipAnimMontage;

  virtual void EndPlay(const EEndPlayReason::Type EndPlayReason) override;
  bool IsAmmoEmpty() const;

  // ====== PUBLIC ======
  public:
  virtual void StartFire();
  void StopFire();
  virtual void NextWeapon();
  void Reload();
  UFUNCTION(BlueprintCallable)
  bool GetWeaponUIData(FWeaponUIData& UIData) const;
  FAmmoData GetAmmoData() const;
  bool TryToAddAmmo(TSubclassOf<ATPSWeapon> WeaponType, int32 ClipsAmmo);
  UPROPERTY()
  ATPSWeapon* CurrentWeapon = nullptr;
  UPROPERTY()
  TArray<ATPSWeapon*> Weapons;
  bool CanFire() const;
  bool CanEquip() const;
  void EquipWeapon(int32 WeaponIndex);
  int32 CurrentWeaponIndex = 0;
  bool NeedAmmo(TSubclassOf<ATPSWeapon> WeaponClass);

  void Zoom(bool Enabled);

  // ====== PRIVATE ======
  private:
  void PlayAnimMontage(UAnimMontage* Animation);

  void SpawnWeapons();

  UPROPERTY()
  UAnimMontage* CurrentReloadAnimMontage;

  UTPSGameInstance* GameInstance;

  bool ReloadAnimInProgress = false;
  bool EquipAnimInProgress = false;

  bool CanReload() const;

  void AttachWeaponToSocket(ATPSWeapon* Weapon, USceneComponent* Mesh, const FName& SocketName);

  void InitAnimations();
  void OnEquipFinished(USkeletalMeshComponent* MeshComp);
  void OnReloadFinished(USkeletalMeshComponent* MeshComp);

  void OnEmptyClip(ATPSWeapon* Weapon);
  void ChangeClip();

  protected:
  FTimerHandle ExploseSoundHandle;

  public:
  UFUNCTION()
  virtual void ToDefaultSMix();
  UFUNCTION(BlueprintCallable)
  virtual void ExploseSound();
  UPROPERTY(EditDefaultsOnly, Category = "Sound")
  float DelayToDefSMix = 4.0f;

  //

  //*/
};
