// https://m.vk.com/id357239644

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "Develop/Types.h"
#include "TimerManager.h"
#include "HealthComponent.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FDelDamage, EDamageType, Type, FDebuff, Debuff);
DECLARE_MULTICAST_DELEGATE(FOnDeath);
DECLARE_MULTICAST_DELEGATE_TwoParams(FOnChangeHealth, float, float);

class UCameraShakeBase;
class AController;
class UPhysicalMaterial;

UCLASS(ClassGroup = (Custom), meta = (BlueprintSpawnableComponent))
class UDEMYTPS_API UHealthComponent : public UActorComponent
{
  GENERATED_BODY()

  public:
  UHealthComponent();

  FDelDamage DelDamage;
  FOnDeath OnDeath;
  FOnChangeHealth OnChangeHealth;

  // ===== PROTECTED ======
  protected:
  virtual void BeginPlay() override;

  UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Health")
  // ��������� �����
  TMap<UPhysicalMaterial*, float> DamageModifier;

  //  ====== PRIVATE ======
  private:
  bool bIsDead = false;
  float Health = 0.0f;

  UFUNCTION()
  void AnyDamage(
      AActor* DamagedActor, float Damage, const class UDamageType* DamageType, class AController* InstigatedBy, AActor* DamageCauser);
  UFUNCTION()
  void OnTakePointDamage(AActor* DamagedActor,  //
      float Damage,                             //
      class AController* InstigatedBy,          //
      FVector HitLocation,                      //
      class UPrimitiveComponent* FHitComponent, //
      FName BoneName,                           //
      FVector ShotFromDirection,                //
      const class UDamageType* DamageType,      //
      AActor* DamageCauser);
  UFUNCTION()
  void OnTakeRadialDamage(AActor* DamagedActor, //
      float Damage,                             //
      const class UDamageType* DamageType,      //
      FVector Origin,                           //
      FHitResult HitInfo,                       //
      class AController* InstigatedBy,          //
      AActor* DamageCauser);

  // ��������� �����, ������ ������, ������ KillerContr
  void ApplyDamage(float Damage, AController* InstigatedBy);

  // �������� ������� �� ���������� �������� � ����������� �� ����� �����
  float GetPointDamageModifier(AActor* DamageActor, const FName& BoneName);

  //  REGEN, HEAL
  void Regeneration();
  FTimerHandle RegenHandle;
  bool bRegenFlag = false;
  float RegenTime = 0.0f;
  UPROPERTY(EditDefaultsOnly, Category = "Heal")
  bool bAutoHeal = true;
  UPROPERTY(EditDefaultsOnly, Category = "Heal", meta = (ClampMin = "0.016", ClampMax = "180", EditCondition = "bAutoHeal"))
  float HealUpdateTime = 0.5f;
  UPROPERTY(EditDefaultsOnly, Category = "Heal", meta = (ClampMin = "0.0", ClampMax = "180", EditCondition = "bAutoHeal"))
  float HealDelay = 10.0f;
  UPROPERTY(EditDefaultsOnly, Category = "Heal", meta = (ClampMin = "0.0", ClampMax = "1000000000", EditCondition = "bAutoHeal"))
  float HealRegen = 2;

  //   DAMAGE TYPE
  bool FireFlag = false;      // flags
  bool IceFlag = false;       //
  bool LightFlag = false;     //
  bool DarkFlag = false;      //
  bool PoisonFlag = false;    //
  float FireMaxTime = 1.0f;   // max time
  float IceMaxTime = 1.0f;    //
  float LightMaxTime = 1.0f;  //
  float DarkMaxTime = 1.0f;   //
  float PoisonMaxTime = 1.0f; //
  float FireTime = 0.0f;      // time
  float IceTime = 0.0f;       //
  float LightTime = 0.0f;     //
  float DarkTime = 0.0f;      //
  float PoisonTime = 0.0f;    //
  float FireTick = 0.5f;      // tick
  float IceTick = 0.5f;       //
  float LightTick = 0.5f;     //
  float DarkTick = 0.5f;      //
  float PoisonTick = 0.5f;    //
  float FireTickDmg = 0.0f;   // damage
  float IceTickDmg = 0.0f;    //
  float LightTickDmg = 0.0f;  //
  float DarkTickDmg = 0.0f;   //
  float PoisonTickDmg = 0.0f; //
  void TimerFire();           // functions
  void TimerIce();            //
  void TimerLight();          //
  void TimerDark();           //
  void TimerPoison();         //
  FTimerHandle FireHandle;    // TimerHandle
  FTimerHandle IceHandle;     //
  FTimerHandle LightHandle;   //
  FTimerHandle DarkHandle;    //
  FTimerHandle PoisonHandle;  //

  bool TimerManagerValid = false;

  void PlayCameraShake();

  void Killed(AController* KillerController);

  UPROPERTY()
  //
  AController* KillerContr = nullptr;

  // ====== PUBLIC ======
  public:
  UFUNCTION(BlueprintCallable)
  float GetHealth() const { return Health; }

  UFUNCTION(BlueprintCallable)
  // ���������� % �� ��
  float GetHealthPercent() const { return Health / MaxHealth; }

  UFUNCTION(BlueprintCallable)
  bool GetIsDead() const { return bIsDead; }

  void ChangeHealth(float Damage);

  UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Health", meta = (ClampMin = "1.0", ClampMax = "999999.9"))
  float MaxHealth = 100.0f;

  UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Magic")
  FName FireDbf;
  UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Magic")
  FName IceDbf;
  UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Magic")
  FName LightDbf;
  UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Magic")
  FName DarkDbf;
  UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Magic")
  FName PoisonDbf;

  bool RegenPercent(float Percent);

  protected:
  UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "VFX")
  TSubclassOf<UCameraShakeBase> CameraShake;

  // ��� ��������� ������, ������� ����� ����
  void ReportDamageEvent(float Damage, AController* InstigatedBy);
};
