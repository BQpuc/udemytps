// https://m.vk.com/id357239644

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Develop/Types.h"
#include "BasePickup.generated.h"

class USphereComponent;
class USoundCue;

UCLASS()
class UDEMYTPS_API ABasePickup : public AActor
{
	GENERATED_BODY()
	
public:	
	ABasePickup();

protected:
	virtual void BeginPlay() override;

public:	
	virtual void Tick(float DeltaTime) override;

	virtual void NotifyActorBeginOverlap(AActor* OtherActor) override;

	bool CouldBeTaken() const;

	// ====== PROTRCTED ======
  protected:
  // Components
  UPROPERTY(VisibleAnywhere, Category = "Pickup")
  USphereComponent* CollisionComponent;

  // Sin Move logic


  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Pickup")
  float RespawnTime = 5.0;

  UPROPERTY(EditAnywhere, Category = "Pickup")
  FRotation RandMove;

  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Sound")
  // ���� ��� ������ ������
  USoundCue* TakeSound;

  // ====== PRIVATE ======
  private:
  FVector InitLocation;
  void SinMove();
  void PickupWasTaken();
  void Respawn();
  virtual bool GivePickupTo(APawn* Pawn);

  float RotationYaw = 0.0f;
  void GenerateRotationYaw();
  FTimerHandle L_RespawnTH;
};
