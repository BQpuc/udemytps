// https://m.vk.com/id357239644

#pragma once

#include "CoreMinimal.h"
#include "Components/Pickups/BasePickup.h"
#include "AmmoPickup.generated.h"

class ATPSWeapon;

UCLASS()
class UDEMYTPS_API AAmmoPickup : public ABasePickup
{
  GENERATED_BODY()


  // ====== PROTECTED ======
  protected:
  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Pickup", meta = (ClampMin = "1", ClampMax = "10"))
  int32 ClipsAmount = 10;
  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Pickup")
  TSubclassOf<ATPSWeapon> WeaponType;


  // ====== PRIVATE ======
  private:
  virtual bool GivePickupTo(APawn* Pawn) override;
};
