// https://m.vk.com/id357239644

#pragma once

#include "CoreMinimal.h"
#include "Components/Pickups/BasePickup.h"
#include "HealthPickup.generated.h"


UCLASS()
class UDEMYTPS_API AHealthPickup : public ABasePickup
{
	GENERATED_BODY()
	
private:
  virtual bool GivePickupTo(APawn* Pawn) override;


protected:
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Pickup")
    float myVariable = 0.5f;
};
