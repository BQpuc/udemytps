// https://m.vk.com/id357239644

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "Develop/Types.h"
#include "WeaponFXComponent.generated.h"

class UNiagaraSystem;
class UPhysicalMaterial;

UCLASS(ClassGroup = (Custom), meta = (BlueprintSpawnableComponent))
class UDEMYTPS_API UWeaponFXComponent : public UActorComponent
{
  GENERATED_BODY()

  public:
  // Sets default values for this component's properties
  UWeaponFXComponent();

  protected:
  // Called when the game starts
  virtual void BeginPlay() override;

  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "VFX") //
  FImpactData DefaultImpactData;                                // DefaultEffect
  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "VFX") //
  TMap<UPhysicalMaterial*, FImpactData> ImpactDataMap;          // EffectMap


  public:
  void PlayImpactFX(const FHitResult& Hit);

};
