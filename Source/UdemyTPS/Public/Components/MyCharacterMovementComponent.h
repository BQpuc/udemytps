// https://m.vk.com/id357239644

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "MyCharacterMovementComponent.generated.h"

class ATPSCharacter;
class UPhysicalMaterial;
class USoundCue;
class USoundAttenuation;

UCLASS()
class UDEMYTPS_API UMyCharacterMovementComponent : public UCharacterMovementComponent
{
  GENERATED_BODY()

  public:
  virtual float GetMaxSpeed() const override;

  UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Movement", meta = (ClampMin = "1", ClampMax = "20"))
  float MaxSpeedRun = 1.2f;

  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Sound")
  // ���� ����� � ����������� �� ����������� ���������
  TMap<UPhysicalMaterial*, USoundCue*> FootSound;

  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Sound")
  USoundCue* DefSound;

  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Sound")
  USoundAttenuation* StepAttenuation;

  UFUNCTION(BlueprintCallable)
  void PlayStepSound(const FHitResult& Hit);

  protected:
  virtual void BeginPlay() override;
};
