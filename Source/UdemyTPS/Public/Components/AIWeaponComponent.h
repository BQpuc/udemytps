// https://m.vk.com/id357239644

#pragma once

#include "CoreMinimal.h"
#include "Components/WeaponComponent.h"
#include "AIWeaponComponent.generated.h"

/**
 * 
 */
UCLASS()
class UDEMYTPS_API UAIWeaponComponent : public UWeaponComponent
{
	GENERATED_BODY()
	
public:
  virtual void StartFire() override;
  virtual void NextWeapon() override;

  virtual void ToDefaultSMix() override;

  virtual void ExploseSound() override;

};
