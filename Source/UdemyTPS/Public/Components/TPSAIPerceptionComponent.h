// https://m.vk.com/id357239644

#pragma once

#include "CoreMinimal.h"
#include "Perception/AIPerceptionComponent.h"
#include "Develop/Types.h"
#include "TPSAIPerceptionComponent.generated.h"

/**
 *
 */
UCLASS()
class UDEMYTPS_API UTPSAIPerceptionComponent : public UAIPerceptionComponent
{
  GENERATED_BODY()

  public:
	  // ���������� ���������� �����
  AActor* GetClosestEnemy() const;

};
